<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

// V1

Route::post('/4dcaf58a1a6fd3555d9626cfadc42510/auth', 'AppController@auth');
Route::post('/4dcaf58a1a6fd3555d9626cfadc42510/reg', 'AppController@register');
Route::post('/4dcaf58a1a6fd3555d9626cfadc42510/addcard', 'AppController@addCard');
Route::post('/4dcaf58a1a6fd3555d9626cfadc42510/fbauth', 'AppController@fbAuth');

Route::post('/4dcaf58a1a6fd3555d9626cfadc42510/forgot-pass', 'AppController@forgotPassword');
Route::post('/4dcaf58a1a6fd3555d9626cfadc42510/new-pass', 'AppController@newPassword');

// V2
Route::post('/v2/4dcaf58a1a6fd3555d9626cfadc42510/auth', 'AppV2Controller@auth');
Route::get('/v2/4dcaf58a1a6fd3555d9626cfadc42510/user', 'AppV2Controller@user');
Route::post('/v2/4dcaf58a1a6fd3555d9626cfadc42510/register', 'AppV2Controller@register');
Route::post('/v2/4dcaf58a1a6fd3555d9626cfadc42510/addcard', 'AppV2Controller@addCard');
Route::post('/v2/4dcaf58a1a6fd3555d9626cfadc42510/fbauth', 'AppV2Controller@fbAuth');

Route::post('/v2/4dcaf58a1a6fd3555d9626cfadc42510/forgot-pass', 'AppV2Controller@forgotPassword');
Route::post('/v2/4dcaf58a1a6fd3555d9626cfadc42510/new-pass', 'AppV2Controller@newPassword');

Route::get('/v2/4dcaf58a1a6fd3555d9626cfadc42510/restaurants', 'AppV2Controller@restaurants');
Route::post('/v2/4dcaf58a1a6fd3555d9626cfadc42510/restaurants', 'AppV2Controller@restaurants');