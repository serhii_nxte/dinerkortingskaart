<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return Redirect::to('/home');
});

Auth::routes();

Route::get('/register', function () {
    return Redirect::to('/home');
});

Route::get('/home', 'HomeController@index');
Route::resource('/users', 'UserController');
Route::get('/cards/import', 'CardController@import');
Route::post('/cards/import', 'CardController@importFunction');
Route::resource('/cards', 'CardController');
Route::post('/cards/filtered', 'CardController@filter');
Route::resource('/partners', 'PartnerController');
Route::get('/restaurants/import', 'RestaurantController@import');
Route::post('/restaurants/import', 'RestaurantController@importFunction');
Route::resource('/restaurants', 'RestaurantController');