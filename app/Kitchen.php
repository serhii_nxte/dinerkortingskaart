<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Kitchen extends Model
{
	/**
     * Get the restaurants
     */
    public function restaurants()
    {
        return $this->belongsTo('App\Restaurant');
    }
}
