<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Card extends Model
{
    /**
	 * Get the post that owns the comment.
	 */
	public function partner()
	{
	    return $this->belongsTo('App\Partner');
	}

	/**
	 * Get the post that owns the comment.
	 */
	public function user()
	{
	    return $this->belongsTo('App\User');
	}

	/**
	 * Check if the card is expired
	 */
	public function isExpired()
	{
		if(strtotime($this->expiration)<strtotime("today")){
			return true; // the card is expired
		}else{
			return false; // the card is valid
		}
	}
}
