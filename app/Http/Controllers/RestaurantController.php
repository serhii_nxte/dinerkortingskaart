<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Restaurant;
use App\Kitchen;
use Session;
use Redirect;
use Illuminate\Pagination\LengthAwarePaginator;
use ImageCrunch;
use Validator;

class RestaurantController extends Controller
{
	/**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('admin');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $restaurants = Restaurant::whereNotNull('created_at');
        
        if(isset($_GET['naam'])){
            $restaurants->Where('name', 'like', '%'.$_GET['naam'].'%');
        }
        $restaurants = $restaurants->paginate(15);

    	$curDate = date('Y-m-d');

        return view('restaurants.index')->with('restaurants', $restaurants)->with('curDate', $curDate);
    }

    /**
     * Create new user
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $kitchens = Kitchen::all();
        return view('restaurants.create')->with('kitchens', $kitchens);
    }

    /**
     * Store new user
     *
     * @return \Illuminate\Http\Response
     */
    public function store(request $request)
    {

    	$restaurant = new Restaurant;
    	$restaurant->name = $request->name;
    	$restaurant->address = $request->address;
    	$restaurant->zip = $request->zip;
    	$restaurant->city = $request->city;
      $restaurant->active = (int)$request->active;

    	// Get lat and long by address         
        $address = $request->address . " " . $request->city;
        $prepAddr = str_replace(' ','+',$address);
        $geocode = file_get_contents('https://maps.google.com/maps/api/geocode/json?address='.$prepAddr.'&sensor=false&key=AIzaSyAuBHo8AT9PUasyUjWWCSZkjISuavAip7g');
        $output = json_decode($geocode);
        $restaurant->latitude = $output->results[0]->geometry->location->lat;
        $restaurant->longitude = $output->results[0]->geometry->location->lng;

    	if($request->phone){
    		$restaurant->phone = $request->phone;
    	}
    	if($request->website){
    		$restaurant->website = $request->website;
    	}
    	if($request->description){
    		$restaurant->description = $request->description;
    	}
    	if($request->discount){
    		$restaurant->discount = $request->discount;
    	}
    	if($request->image){
    		$restaurant->image = $request->image;
    	}

      // save kitchen relation
      $kitchen = Kitchen::find($request->kitchen);
      $restaurant->kitchen()->associate($kitchen);

      // store lunch times
      // Monday
      if($request->lunch_from_1 && $request->lunch_to_1){
        $restaurant->lunch_from_1 = $request->lunch_from_1;
        $restaurant->lunch_to_1 = $request->lunch_to_1;
      }
      // Tuesday
      if($request->lunch_from_2 && $request->lunch_to_2){
        $restaurant->lunch_from_2 = $request->lunch_from_2;
        $restaurant->lunch_to_2 = $request->lunch_to_2;
      }
      // Wednesday
      if($request->lunch_from_3 && $request->lunch_to_3){
        $restaurant->lunch_from_3 = $request->lunch_from_3;
        $restaurant->lunch_to_3 = $request->lunch_to_3;
      }
      // Thursday
      if($request->lunch_from_4 && $request->lunch_to_4){
        $restaurant->lunch_from_4 = $request->lunch_from_4;
        $restaurant->lunch_to_4 = $request->lunch_to_4;
      }
      // Friday
      if($request->lunch_from_5 && $request->lunch_to_5){
        $restaurant->lunch_from_5 = $request->lunch_from_5;
        $restaurant->lunch_to_5 = $request->lunch_to_5;
      }
      // Saturday
      if($request->lunch_from_6 && $request->lunch_to_6){
        $restaurant->lunch_from_6 = $request->lunch_from_6;
        $restaurant->lunch_to_6 = $request->lunch_to_6;
      }
      // Sunday
      if($request->lunch_from_7 && $request->lunch_to_7){
        $restaurant->lunch_from_7 = $request->lunch_from_7;
        $restaurant->lunch_to_7 = $request->lunch_to_7;
      }

      // store diner times
      // Monday
      if($request->diner_from_1 && $request->diner_to_1){
        $restaurant->diner_from_1 = $request->diner_from_1;
        $restaurant->diner_to_1 = $request->diner_to_1;
      }
      // Tuesday
      if($request->diner_from_2 && $request->diner_to_2){
        $restaurant->diner_from_2 = $request->diner_from_2;
        $restaurant->diner_to_2 = $request->diner_to_2;
      }
      // Wednesday
      if($request->diner_from_3 && $request->diner_to_3){
        $restaurant->diner_from_3 = $request->diner_from_3;
        $restaurant->diner_to_3 = $request->diner_to_3;
      }
      // Thursday
      if($request->diner_from_4 && $request->diner_to_4){
        $restaurant->diner_from_4 = $request->diner_from_4;
        $restaurant->diner_to_4 = $request->diner_to_4;
      }
      // Friday
      if($request->diner_from_5 && $request->diner_to_5){
        $restaurant->diner_from_5 = $request->diner_from_5;
        $restaurant->diner_to_5 = $request->diner_to_5;
      }
      // Saturday
      if($request->diner_from_6 && $request->diner_to_6){
        $restaurant->diner_from_6 = $request->diner_from_6;
        $restaurant->diner_to_6 = $request->diner_to_6;
      }
      // Sunday
      if($request->diner_from_7 && $request->diner_to_7){
        $restaurant->diner_from_7 = $request->diner_from_7;
        $restaurant->diner_to_7 = $request->diner_to_7;
      }

    	$restaurant->save();


        // if has image
        if($request->hasFile('image')){
            $file = $request->file('image');
            $rules = array('file' => 'required|mimes:png,gif,jpeg', 'file' => 'max:3200');
            $validator = Validator::make(array('file'=> $file), $rules);
            if($validator->passes()){
              $filename = substr( md5($restaurant->id.time()), 0, 7).".".$file->getClientOriginalExtension();
              $path = public_path("images/uploads/restaurants/".$filename);
              ImageCrunch::make($file)->resize(580, 200, function ($constraint) {$constraint->aspectRatio();})->save($path);
              // save image
              $restaurant->image = url('/')."/images/uploads/restaurants/".$filename;
              $restaurant->save();
            }else{
              Session::flash("warning_message", "ERROR!!<br/>File formats permitted: PNG, GIF, JPG.<br/>Max file size: ±3,2MB");
              return Redirect::back()->withInput();
            }
        }

    	Session::flash("succes_message", "Restaurant <strong>$restaurant->name</strong> is aangemaakt.");
    	return Redirect::to('/restaurants');
    }

    /**
     * Create new user
     *
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
      $kitchens = Kitchen::all();
    	$restaurant = Restaurant::find($id);
      return view('restaurants.edit')->with('restaurant', $restaurant)->with('kitchens', $kitchens);
    }

    /**
     * Store new user
     *
     * @return \Illuminate\Http\Response
     */
    public function update($id, request $request)
    {

    	$restaurant = Restaurant::find($id);
    	$restaurant->name = $request->name;
    	$restaurant->address = $request->address;
    	$restaurant->zip = $request->zip;
    	$restaurant->city = $request->city;
      $restaurant->active = (int)$request->active;

    	// Get lat and long by address         
        $address = $request->address . " " . $request->city;
        $prepAddr = str_replace(' ','+',$address);
        $geocode = file_get_contents('https://maps.google.com/maps/api/geocode/json?address='.$prepAddr.'&sensor=false&key=AIzaSyAuBHo8AT9PUasyUjWWCSZkjISuavAip7g');
        $output = json_decode($geocode);
        $restaurant->latitude = $output->results[0]->geometry->location->lat;
        $restaurant->longitude = $output->results[0]->geometry->location->lng;

    	if($request->phone){
    		$restaurant->phone = $request->phone;
    	}
    	if($request->website){
    		$restaurant->website = $request->website;
    	}
    	if($request->description){
    		$restaurant->description = $request->description;
    	}
    	if($request->discount){
    		$restaurant->discount = $request->discount;
    	}
    	if($request->image){
    		$restaurant->image = $request->image;
    	}
    	$restaurant->save();

        // if has image
        if($request->hasFile('image')){
            $file = $request->file('image');
            $rules = array('file' => 'required|mimes:png,gif,jpeg', 'file' => 'max:3200');
            $validator = Validator::make(array('file'=> $file), $rules);
            if($validator->passes()){
              $filename = substr( md5($restaurant->id.time()), 0, 7).".".$file->getClientOriginalExtension();
              $path = public_path("images/uploads/restaurants/".$filename);
              ImageCrunch::make($file)->resize(580, NULL, function ($constraint) {$constraint->aspectRatio();})->save($path);
              // save image
              $restaurant->image = url('/')."/images/uploads/restaurants/".$filename;
              $restaurant->save();
            }else{
              Session::flash("warning_message", "ERROR!!<br/>File formats permitted: PNG, GIF, JPG.<br/>Max file size: ±3,2MB");
              return Redirect::back()->withInput();
            }
        }

    	Session::flash("succes_message", "Restaurant <strong>$restaurant->name</strong> is aangemaakt.");
    	return Redirect::to('/restaurants');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function import()
    {
        return view('restaurants.import');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function importFunction(request $request)
    {

        $kitchensAll = Kitchen::all();
        $kitchenArray = array();

        foreach ($kitchensAll as $kitchen) {
          $kitchenArray[$kitchen->id] = $kitchen->name;
        }

        $file = file(strip_tags($request->import));

        // CSV to array
        foreach($file as $k){
            $csv[] = explode(';', $k);
        }

        $i=0;
        $imported=0;
        $ignored=0;
        foreach ($csv as $value) {
            $i++;
              if($i !== 1){

              if(!Restaurant::where('name', $value[0])->count()){
                  echo $value[0]."<br/>";
                  $restaurant = new Restaurant;
                  $restaurant->name = $value[0];
                  $restaurant->address = $value[12];
                  $restaurant->zip = $value[10];
                  $restaurant->city = $value[8];
                  if($value[14] !== "" && $value[14] !== " "){
                    $restaurant->phone = $value[14];
                  }
                  if($value[3] !== "" && $value[3] !== " "){
                    $restaurant->website = $value[3];
                  }
                  $restaurant->longitude = $value[7];
                  $restaurant->latitude = $value[6];
                  $restaurant->description = $value[1];
                  $restaurant->discount = null;

                  // kitchen
                  $key = array_search($value[5], $kitchenArray);
                  if ($key !== false) {
                    $restaurant->kitchen_id = $key;
                  }

                  // image
                  $restaurant->image = $value[2];

                  // times lunch
                  if($value[15] !== "" && $value[15] !== " "){
                    $restaurant->lunch_from_1 = preg_replace('/\s+/', '', $value[15]);
                  }
                  if($value[16] !== "" && $value[16] !== " "){
                    $restaurant->lunch_to_1 = preg_replace('/\s+/', '', $value[16]);
                  }
                  if($value[17] !== "" && $value[17] !== " "){
                    $restaurant->lunch_from_2 = preg_replace('/\s+/', '', $value[17]);
                  }
                  if($value[18] !== "" && $value[18] !== " "){
                    $restaurant->lunch_to_2 = preg_replace('/\s+/', '', $value[18]);
                  }
                  if($value[19] !== "" && $value[19] !== " "){
                    $restaurant->lunch_from_3 = preg_replace('/\s+/', '', $value[19]);
                  }
                  if($value[20] !== "" && $value[20] !== " "){
                    $restaurant->lunch_to_3 = preg_replace('/\s+/', '', $value[20]);
                  }
                  if($value[21] !== "" && $value[21] !== " "){
                    $restaurant->lunch_from_4 = preg_replace('/\s+/', '', $value[21]);
                  }
                  if($value[22] !== "" && $value[22] !== " "){
                    $restaurant->lunch_to_4 = preg_replace('/\s+/', '', $value[22]);
                  }
                  if($value[23] !== "" && $value[23] !== " "){
                    $restaurant->lunch_from_5 = preg_replace('/\s+/', '', $value[23]);
                  }
                  if($value[24] !== "" && $value[24] !== " "){
                    $restaurant->lunch_to_5 = preg_replace('/\s+/', '', $value[24]);
                  }
                  if($value[25] !== "" && $value[25] !== " "){
                    $restaurant->lunch_from_6 = preg_replace('/\s+/', '', $value[25]);
                  }
                  if($value[26] !== "" && $value[26] !== " "){
                    $restaurant->lunch_to_6 = preg_replace('/\s+/', '', $value[26]);
                  }
                  if($value[27] !== "" && $value[27] !== " "){
                    $restaurant->lunch_from_7 = preg_replace('/\s+/', '', $value[27]);
                  }
                  if($value[28] !== "" && $value[28] !== " "){
                    $restaurant->lunch_to_7 = preg_replace('/\s+/', '', $value[28]);
                  }


                  // times diner
                  if($value[29] !== "" && $value[29] !== " "){
                    $restaurant->diner_from_1 = preg_replace('/\s+/', '', $value[29]);
                  }
                  if($value[30] !== "" && $value[30] !== " "){
                    $restaurant->diner_to_1 = preg_replace('/\s+/', '', $value[30]);
                  }
                  if($value[31] !== "" && $value[31] !== " "){
                    $restaurant->diner_from_2 = preg_replace('/\s+/', '', $value[31]);
                  }
                  if($value[32] !== "" && $value[32] !== " "){
                    $restaurant->diner_to_2 = preg_replace('/\s+/', '', $value[32]);
                  }
                  if($value[33] !== "" && $value[33] !== " "){
                    $restaurant->diner_from_3 = preg_replace('/\s+/', '', $value[33]);
                  }
                  if($value[34] !== "" && $value[34] !== " "){
                    $restaurant->diner_to_3 = preg_replace('/\s+/', '', $value[34]);
                  }
                  if($value[35] !== "" && $value[35] !== " "){
                    $restaurant->diner_from_4 = preg_replace('/\s+/', '', $value[35]);
                  }
                  if($value[36] !== "" && $value[36] !== " "){
                    $restaurant->diner_to_4 = preg_replace('/\s+/', '', $value[36]);
                  }
                  if($value[37] !== "" && $value[37] !== " "){
                    $restaurant->diner_from_5 = preg_replace('/\s+/', '', $value[37]);
                  }
                  if($value[38] !== "" && $value[38] !== " "){
                    $restaurant->diner_to_5 = preg_replace('/\s+/', '', $value[38]);
                  }
                  if($value[39] !== "" && $value[39] !== " "){
                    $restaurant->diner_from_6 = preg_replace('/\s+/', '', $value[39]);
                  }
                  if($value[40] !== "" && $value[40] !== " "){
                    $restaurant->diner_to_6 = preg_replace('/\s+/', '', $value[40]);
                  }
                  if($value[41] !== "" && $value[41] !== " "){
                    $restaurant->diner_from_7 = preg_replace('/\s+/', '', $value[41]);
                  }
                  if($value[42] !== "" && $value[42] !== " "){
                    $restaurant->diner_to_7 = preg_replace('/\s+/', '', $value[42]);
                  }

                  $imported++;
                  $restaurant->save();
              }else{
                $ignored++;
                echo "Restaurant with the same name already exists<br/>";
              }
           }
        }

        Session::flash("succes_message", "$imported restaurants succesvol aangemaakt. $ignored regels genegeerd (restaurant met dezelfde naam bestaat al).");
        return Redirect::to('/restaurants');
    }

}
