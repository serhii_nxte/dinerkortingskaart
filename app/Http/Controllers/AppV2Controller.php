<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use App\Card;
use App\User;
use App\Partner;
use App\Restaurant;
use Hash;
use Mail;
use App\Mail\passwordRecovery;
use JWTAuth;
use Tymon\JWTAuth\Exceptions\JWTException;
use url;
use App\Kitchen;

class AppV2Controller extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function auth(request $request)
    { 

        // grab credentials from the request
        $credentials = $request->only('email', 'password');
        try {
            // attempt to verify the credentials and create a token for the user
            if ($token = JWTAuth::attempt($credentials)) {
                // return jwt
                $jwt = response()->json(compact('token'));
                $code = 200;
                $jwt = $token;
                $message = NULL;
            }else{
                // wrong credentials / no user found
                $code = 500;
                $jwt = NULL;
                $message = 'Email of wachtwoord is incorrect.';
            }
        } catch (JWTException $e) {
            // something went wrong
            $code = 500;
            $jwt = NULL;
            $message = 'Email of wachtwoord is incorrect.';
        }

        $data = NULL;

        $response = array(
            'code' => $code,
            'jwt' => $jwt,
            'data' => $data,
            'message' => $message
        );

        // all good so return the token
        return json_encode($response);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    function user(){

        $jwt = $_GET['token'];
        if($user = JWTAuth::parseToken()->authenticate()){

            //check if user has valid card
            if($user->cards->count()){
                
                // user has valid card
                $card = $user->cards()->orderBy('expiration', 'desc')->first();
                $partnerImage = NULL;
                $partnertext = NULL;
                if($card->partner){
                    $partnerImage = url('/')."/images/uploads/partners/".$card->partner->image;
                    if($card->partner->tekst){
                        $partnertext = $card->partner->tekst;
                    }
                }

                // collect data
                $data = array(
                    'fname' => $user->fname,
                    'lname' => $user->lname,
                    'email' => $user->email,
                    'expiration' => date('d-m-Y', strtotime($card->expiration)),
                    'card' => $card->cardnumber,
                    'partner_image' => $partnerImage,
                    'partner_text' => $partnertext,
                    'nocard' => NULL
                );

                // check if card is expired
                if($card->isExpired()){
                    $data['nocard'] = 1; // the card is expired
                    $message = "Uw kaart (".$card->cardnumber.") is verlopen op: ".date('d-m-Y', strtotime($card->expiration));
                }

                $code = 200;
                $message = NULL;

            }else{

                // user has no valid card
                $data = array(
                    'fname' => $user->fname,
                    'lname' => $user->lname,
                    'email' => $user->email,
                    'expiration' => NULL,
                    'card' => NULL,
                    'partner_image' => NULL,
                    'partner_text' => NULL,
                    'nocard' => 1
                );

                $code = 200;
                $message = 'Uw account heeft nog geen kaart.';

            }

        }else{
            $code = 503;
            $jwt = NULL;
            $data = NULL;
            $message = 'Uw inlog sessie is mogelijk verlopen. Probeer nogmaals in te loggen.';
        }

        $response = array(
            'code' => $code,
            'jwt' => $jwt,
            'data' => $data,
            'message' => $message
        );

        return json_encode($response);

    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function register(request $request)
    {
        $curdate = date('Y-m-d');
        $jwt = NULL;
        $data = NULL;
        $message = NULL;
        $code = 200;

        // check if card exists
        if(Card::where('activation', $request->code)->where('expiration', '>=', $curdate)->whereNull('user_id')->count() || Card::where('activation', $request->code)->whereNull('expiration')->whereNull('user_id')->count()){

            // check if email exists
            if(!User::where('email', $request->email)->count()){

                // check if email is valid
                if(filter_var($request->email, FILTER_VALIDATE_EMAIL)){

                    // password check
                    if($request->password == $request->password_repeat){
                        if(strlen($request->password) > 5){

                            // create user
                            $user = new User;
                            $user->fname = $request->fname;
                            $user->lname = $request->lname;
                            $user->email = $request->email;
                            $user->password = Hash::make($request->password);
                            $user->save();
                            
                            // add card to user
                            $card = Card::where('activation', $request->code)->first();
                            if(is_null($card->expiration)){
                                $period = round($card->period * 30.5);
                                $card->expiration = date('Y-m-d', strtotime("+$period days"));
                            }
                            $card->user_id = $user->id;
                            $card->save();
                            
                            // get card and partner data
                            $partnerImage = NULL;
                            $partnertext = NULL;
                            if(!is_null($card->partner)){
                                $partnerImage = url('/')."/images/uploads/partners/".$card->partner->image;
                                if($card->partner->tekst){
                                    $partnertext = $card->partner->tekst;
                                }
                            }
                            
                            // create the data array
                            $data = array(
                                'fname' => $user->fname,
                                'lname' => $user->lname,
                                'email' => $user->email,
                                'expiration' => date('d-m-Y', strtotime($card->expiration)),
                                'card' => $card->cardnumber,
                                'partner_image' => $partnerImage,
                                'partner_text' => $partnertext,
                                'nocard' => NULL
                            );

                            // set response vars
                            $code = 200;
                            $jwt = JWTAuth::fromUser($user); 


                        }else{
                            $code = 500;
                            $message = 'Wachtwoord moet minimaal 6 tekens lang zijn.';
                        }
                    }else{
                        $code = 500;
                        $message = 'Wachtwoord en wachtwoord herhalen zijn niet gelijk.';
                    }
                }else{
                    $code = 500;
                    $message = 'Ongeldig mailadres. Probeer een ander mailadres.';
                }
            }else{
                $code = 500;
                $message = 'Er bestaat al een gebruiker met dit e-mailadres.';
            }
        }else{
            $code = 404;
            $message = 'Kaartnummer / kaartcode is incorrect, al toegewezen aan een account of verlopen.';
        }

        $response = array(
            'code' => $code,
            'jwt' => $jwt,
            'data' => $data,
            'message' => $message
        );

        return json_encode($response);

    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function addCard(request $request)
    {
        $curdate = date('Y-m-d');
        $jwt = NULL;
        $message = NULL;
        $data = NULL;
        // check if card exists
        if(Card::where('activation', $request->code)->where('expiration', '>=', $curdate)->whereNull('user_id')->count() || Card::where('activation', $request->code)->whereNull('expiration')->whereNull('user_id')->count()){
            // auth check
            if ($user = JWTAuth::parseToken()->authenticate()) {

                // Activate card
                $card = Card::where('activation', $request->code)->first();
                $card->user_id = Auth::user()->id;
                if(is_null($card->expiration)){
                    $period = round($card->period * 30.5);
                    $card->expiration = date('Y-m-d', strtotime("+$period days"));
                }
                $card->save();

                // Get card and partner data
                $partnerImage = NULL;
                $partnertext = NULL;
                if($card->partner){
                    $partnerImage = url('/')."/images/uploads/partners/".$card->partner->image;
                    if($card->partner->tekst){
                        $partnertext = $card->partner->tekst;
                    }
                }


                // create the data array
                $data = array(
                    'fname' => $user->fname,
                    'lname' => $user->lname,
                    'email' => $user->email,
                    'expiration' => date('d-m-Y', strtotime($card->expiration)),
                    'card' => $card->cardnumber,
                    'partner_image' => $partnerImage,
                    'partner_text' => $partnertext,
                    'nocard' => NULL
                );

                $code = 200;
                $message = NULL;
                $jwt = JWTAuth::fromUser($user);             

            }else{
                // wrong username or password
                $code = 503;
                $message = 'Email of wachtwoord is incorrect, Ga terug en probeer opnieuw'; 
            }
        }else{
            $code = 404;
            $message = 'Kaartnummer / kaartcode is incorrect, al toegewezen aan een account of verlopen.';
        }

        $response = array(
            'code' => $code,
            'jwt' => $jwt,
            'data' => $data,
            'message' => $message
        );

        return json_encode($response);

    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function fbAuth(request $request)
    {

        // Facebook Response
        $access_token = $request->fbtoken;
        $user_details = "https://graph.facebook.com/me?fields=email,first_name,last_name&access_token=".$access_token;
        $response = file_get_contents($user_details);
        $response = json_decode($response);

        // NULLABLE VARS
        $jwt = NULL;
        $message = NULL;
        $data = NULL;
        $expiration = NULL;
        $card = NULL;
        $partnerImage = NULL;
        $partnertext = NULL;
        $nocard = NULL;

        if(!empty($response->email)){
            if($user = User::where('email', $response->email)->first()){

                // get existing account
                if($user->cards->count()){
                    // get card and partner data
                    $card = $user->cards()->orderBy('expiration', 'desc')->first();
                    if($card->partner){
                        $partnerImage = url('/')."/images/uploads/partners/".$card->partner->image;
                        if($card->partner->tekst){
                            $partnertext = $card->partner->tekst;
                        }
                    }

                    // check if card is expired
                    if($card->isExpired()){
                        $nocard = 1; // the card is expired
                        $message = "Uw vorige kaart (".$card->cardnumber.") is verlopen op: ".date('d-m-Y', strtotime($card->expiration)).".";
                    }

                    // create the data array
                    $expiration = date('d-m-Y', strtotime($card->expiration));
                    $card = $card->cardnumber;

                }else{
                    // NO CARD
                    $nocard = 1;
                    $message = 'Uw account heeft nog geen kaart.';
                }

            }else{
                // create account
                // GENERATE RANDOM PASSWORD
                $chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789!@#$%^&*()_-=+;:,.?";
                $password = substr( str_shuffle( $chars ), 0, 12 );

                // CREATE ACCOUNT
                $user = new User;
                $user->fname = $response->first_name;
                $user->lname = $response->last_name;
                $user->email = $response->email;
                $user->password = Hash::make($password);
                $user->save();
                
                // fill vars
                $nocard = 1;
                $message = 'Uw account heeft nog geen kaart.';

            }

            // Fill data
            $data = array(
                'fname' => $user->fname,
                'lname' => $user->lname,
                'email' => $user->email,
                'expiration' => $expiration,
                'card' => $card,
                'partner_image' => $partnerImage,
                'partner_text' => $partnertext,
                'nocard' => $nocard
            );
            $code = 200;
            $jwt = JWTAuth::fromUser($user); 

        }else{
            // EMAIL COULD NOT BE RETRIEVED FROM FACEBOOK
            $code = 500;
            $message = 'Verficatie met facebook mislukt, probeer het opnieuw of log in met e-mail en wachtwoord.';
        }

        $response = array(
            'code' => $code,
            'jwt' => $jwt,
            'data' => $data,
            'message' => $message
        );

        return json_encode($response);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function forgotPassword(request $request)
    {
        $email = $request->email;
        $user = User::where('email', '=', $email)->first();

        // NULLABLE VARS
        $jwt = NULL;
        $data = NULL;
        $message = NULL;

        if ($user !== null) {
            $recovery = substr(md5(mt_rand()), 0, 7);
            $expire = strtotime("+1 day");
            $expire = date('Y-m-d H:i:s', $expire);

            $user->password_recovery = $recovery;
            $user->password_recovery_expire = $expire;
            $user->save();

            Mail::to($email)->send(new passwordRecovery($user));

            $data = array(
                'fname' => NULL,
                'lname' => NULL,
                'email' => $user->email,
                'expiration' => NULL,
                'card' => NULL,
                'partner_image' => NULL,
                'partner_text' => NULL,
                'nocard' => NULL
            );
            $code = 200;
            $jwt = NULL;

        }else{
            $code = 500;
            $message = 'Er is geen gebruiker met het opgegeven e-mailadres gevonden.'; 
        }

        $response = array(
            'code' => $code,
            'jwt' => $jwt,
            'data' => $data,
            'message' => $message
        );

        return json_encode($response);

    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function newPassword(request $request)
    {

        // NULLABLE VARS
        $jwt = NULL;
        $data = NULL;
        $message = NULL;
        $nocard = NULL;
        $partnerImage = NULL;
        $partnertext = NULL;
        $card = NULL;
        $expiration = NULL;

        if($request->password == $request->password_repeat){

            $email = $request->email;
            $recovery = strtolower($request->code);
            $password = strtolower($request->password);

            $user = User::where('email', '=', $email)->first();
            if ($user !== null) {
                if($user->password_recovery == $recovery && strtotime(Date('Y-m-d H:i:s')) <= strtotime($user->password_recovery_expire)){

                    // change password
                    $user->password = Hash::make($password);
                    $user->password_recovery = NULL;
                    $user->password_recovery_expire = NULL;
                    $user->save();

                    if($user->cards->count()){

                        // get card and partner data
                        $card = $user->cards()->orderBy('expiration', 'desc')->first();
                        if($card->partner){
                            $partnerImage = url('/')."/images/uploads/partners/".$card->partner->image;
                            if($card->partner->tekst){
                                $partnertext = $card->partner->tekst;
                            }
                        }

                        if($card->isExpired()){
                            $nocard = 1; // the card is expired
                            $message = "Uw vorige kaart (".$card->cardnumber.") is verlopen op: ".date('d-m-Y', strtotime($card->expiration));
                        }

                        // create the data array
                        $expiration = date('d-m-Y', strtotime($card->expiration));
                        $card = $card->cardnumber;


                        $message = 'Uw wachtwoord is aangepast.';

                    }else{
                        
                        $nocard = 1;
                        $message = 'Uw account heeft nog geen kaart.';

                    }

                    // Fill data
                    $data = array(
                        'fname' => $user->fname,
                        'lname' => $user->lname,
                        'email' => $user->email,
                        'expiration' => $expiration,
                        'card' => $card,
                        'partner_image' => $partnerImage,
                        'partner_text' => $partnertext,
                        'nocard' => $nocard
                    );
                    $code = 200;
                    $jwt = JWTAuth::fromUser($user);

                }else{
                    $code = 500;
                    $message = 'Code is incorrect of niet langer geldig. Probeer het opnieuw.';
                }
            }else{
                $code = 500;
                $message = 'Er is geen gebruiker met het opgegeven e-mailadres gevonden.'; 
            }

        }else{
            $code = 500;
            $message = 'Wachtwoord en wachtwoord herhalen zijn niet gelijk.';
        }
        
        $response = array(
            'code' => $code,
            'jwt' => $jwt,
            'data' => $data,
            'message' => $message
        );

        return json_encode($response);
        
    }

    /**
     * Call restaurants
     *
     * @return \Illuminate\Http\Response
     */
    public function restaurants(request $request)
    { 
        // set vars
        $method = $request->method();
        $code = 200;
        $message = NULL;
        $jwt = NULL;
        $data = array();

        // kitchen array
        $kitchens = Kitchen::all();
        $kitchenArray = array();
        foreach ($kitchens as $kitchen) {
            $kitchenArray[$kitchen->id] = $kitchen->name;
        }

        // check if has JWT and set it
        if(isset($_GET['token'])){
            if ($user = JWTAuth::parseToken()->authenticate()) {
                $jwt = JWTAuth::fromUser($user);
            }
        }
        

        // search action
        if ($request->isMethod('post')) {

            // if filter by name
            if(isset($request->location)){
                $location = str_replace(" ", "+", $request->location);
                $locationUrl = "https://maps.googleapis.com/maps/api/geocode/json?address=".$location."+netherlands&key=AIzaSyAuBHo8AT9PUasyUjWWCSZkjISuavAip7g";
                $response = file_get_contents($locationUrl);
                $response = json_decode($response);
                if(isset($response->results[0])){

                    // get the zoom
                    if($response->results[0]->address_components[0]->types[0] == '' || $response->results[0]->address_components[0]->types[0] == ''){
                        $data['zoom'] = 16;
                    }else{
                        $data['zoom'] = 13;
                    }

                    // get to lat & long
                    if($response->results[0]->geometry->location->lat !== '52.132633' && $response->results[0]->geometry->location->lng !== '5.291265999999999'){
                        $data['latitude'] = $response->results[0]->geometry->location->lat;
                        $data['longitude'] = $response->results[0]->geometry->location->lng;
                    }else{
                        $code = 404;
                        $message = 'Er is geen locatie gevonden op basis van de opgegeven invoer, probeer het nogmaals.';
                    }
                    
                }else{
                    $code = 404;
                    $message = 'Er is geen locatie gevonden op basis van de opgegeven invoer, probeer het nogmaals.';
                }
            }elseif(isset($request->name)){
                $restaurant = Restaurant::where('name', 'like', '%' . $request->name . '%')->where('active', 1)->first();
                if(!$restaurant){
                    $code = 404;
                    $message = 'Er is geen restaurant gevonden op basis van de opgegeven invoer, probeer het nogmaals.';
                }else{
                    $data['latitude'] = $restaurant->latitude;
                    $data['longitude'] = $restaurant->longitude;
                    $data['restaurant'] = $restaurant->id;
                    $data['zoom'] = 16;
                }
            }else{
                $code = 404;
                $message = 'Er is geen locatie gevonden op basis van de opgegeven invoer, probeer het nogmaals.';
            }
            
        }else{
            // get all restaurants
            $restaurants = Restaurant::where('active', 1)->get();

            foreach ($restaurants as $restaurant) {
                if(strpos($restaurant->image, ',') !== false){
                    $img = explode(',', $restaurant->image); $img = $img[0];
                    $restaurant->image = $img;
                }
                if(!$restaurant->discount){
                    $restaurant->discount = 'Bij dit restaurant zijn onze algemene voorwaarden van toepassing.';
                }
                $restaurant->description = str_replace('\n', '<br/>', $restaurant->description);
                $restaurant->days = $restaurant->days();
                if(!is_null($restaurant->kitchen_id)){
                    $restaurant->kitchen = $kitchenArray[$restaurant->kitchen_id];
                }else{
                    $restaurant->kitchen = null;
                }
            }

            $data = $restaurants;
        }

        // prepare response
        $response = array(
            'code' => $code,
            'jwt' => $jwt,
            'data' => $data,
            'message' => $message
        );

        //response
        return json_encode($response);
    }

}
