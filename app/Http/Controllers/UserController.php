<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\User;
use App\Card;
use App\Partner;
use Auth;
use Redirect;
use Session;
use Hash;

class UserController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $user = User::where('admin', 0);
        
        if(isset($_GET['email'])){
            $user->Where('email', 'like', '%'.$_GET['email'].'%');
        }
        if(isset($_GET['naam'])){
            $user->Where('lname', 'like', '%'.$_GET['naam'].'%')->orWhere('fname', 'like', '%'.$_GET['naam'].'%');
        }
        if(isset($_GET['email'])){
            $user->Where('email', 'like', '%'.$_GET['email'].'%');
        }

    	$users = $user->paginate(12);
    	$curDate = date('Y-m-d');

        return view('users.index')
        		->with('users', $users)
        		->with('curDate', $curDate);
    }

    /**
     * Create new user
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('users.create');
    }

    /**
     * Store new user
     *
     * @return \Illuminate\Http\Response
     */
    public function store(request $request)
    {
    	// check if all data is filled
    	if($request->fname && $request->lname && $request->password && $request->passwordrepeat && $request->activationcode){
    		// check if password & password repeat are identical
    		if($request->passwordrepeat == $request->password){
    			// check if card (activationcode) exists
    			if($card = Card::where('activation', $request->activationcode)->where('user_id', NULL)->first()){
    				// check if card is expired
    				if(date('Y-m-d') < $card->expiration){
    					
    					// create the user
    					$user = new User;
    					$user->fname = $request->fname;
    					$user->lname = $request->lname;
    					$user->email = $request->email;
    					$user->password = Hash::make($request->password);
    					$user->save();

    					// add user to card
    					$card->user_id = $user->id;
    					$card->save();

    					Session::flash("succes_message", "Gebruiker <strong>$user->fname $user->lname</strong> is aangemaakt.");
    					return Redirect::to('/users');

    				}else{
    					Session::flash("error_message", "Ongeldige activatie code. (kaart verlopen)");
    					return Redirect::back()->withInput($request->input());
    				}
    				
    			}else{
    				Session::flash("error_message", "Ongeldige activatie code.");
    				return Redirect::back()->withInput($request->input());
    			}
    		}else{
    			Session::flash("error_message", "Wachtwoord en wachtwoord herhalen waren niet identiek aan elkaar.");
    			return Redirect::back()->withInput($request->input());
    		}
    	}else{
    		Session::flash("error_message", "Niet alle velden zijn correct ingevoerd.");
    		return Redirect::back()->withInput($request->input());
    	}
    }

    /**
     * Edit user
     *
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
    	$user = User::find($id);
        return view('users.edit')
        			->with('user', $user);
    }

    /**
     * Store new user
     *
     * @return \Illuminate\Http\Response
     */
    public function update($id, request $request)
    {
    	// check if all data is filled
    	if($request->fname && $request->lname && $request->email){

    			$user = User::find($id);
    			$user->fname = $request->fname;
				$user->lname = $request->lname;
				$user->email = $request->email;

    			if($request->activationcode){
    				if($card = Card::where('activation', $request->activationcode)->where('user_id', NULL)->first() && date('Y-m-d') < $card->expiration){
    					$card->user_id = $id;
    					$card->save();
    				}
    			}

    			if($request->password && $request->passwordrepeat && $request->password == $request->passwordrepeat){
    				$user->password = Hash::make($request->password);
    			}

    			$user->save();

    			Session::flash("succes_message", "Gebruiker <strong>$user->fname $user->lname</strong> is aangepast.");
    			return Redirect::to('/users');

    	}else{
    		Session::flash("error_message", "Niet alle velden zijn correct ingevoerd.");
    		return Redirect::back()->withInput($request->input());
    	}
    }

}
