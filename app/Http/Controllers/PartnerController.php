<?php

namespace App\Http\Controllers;

use App\Partner;
use Illuminate\Http\Request;

use App\User;
use App\Card;
use Auth;
use Redirect;
use Session;
use ImageProcessor;
use Input;
use Validator;

class PartnerController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $partners = Partner::all();
        return view('partners.index')
                ->with('partners', $partners);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('partners.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        if($request->file('image')){
            $partner = new Partner;
            $partner->name = $request->name;
            if($request->text){
                $partner->tekst = $request->text;
            }
        }else{
            Session::flash("error_message", "Afbeelding ontbreekt");
            return Redirect::back()->withInput($request->input());
        }


        $filename = preg_replace('/[^A-Za-z0-9\-]/', '', str_replace(' ', '-', $request->name)); // Remove special chars & replace spaces with dashes

        $file = $request->file('image');
        $rules = array('file' => 'required|mimes:png,gif,jpeg', 'file' => 'max:3200');
        $validator = Validator::make(array('file'=> $file), $rules);
        if($validator->passes()){

          $partner->save();

          $filename = $filename."_".$partner->id.".".$request->image->extension();
          $path = public_path("images/uploads/partners/".$filename);
          ImageProcessor::make($file)->resize(480, null, function ($constraint) {$constraint->aspectRatio();})->save($path);
          
          // save image & partner
          $partner->image = $filename;
          $partner->save();

           Session::flash("succes_message", "Partner <strong>$partner->name</strong> is aangemaakt.");
           return Redirect::to('/partners');

        }else{
            Session::flash("error_message", "Afbeelding moet een .jpg, .png of .gif zijn en mag maximaal 3MB groot zijn.");
            return Redirect::back()->withInput($request->input());
        }

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Partner  $partner
     * @return \Illuminate\Http\Response
     */
    public function show(Partner $partner)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Partner  $partner
     * @return \Illuminate\Http\Response
     */
    public function edit(Partner $partner)
    {
        $partner = Partner::find($partner)->first();
        return view('partners.edit')
                ->with('partner', $partner);

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Partner  $partner
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Partner $partner)
    {
        
        $partner = Partner::find($partner)->first();
        $partner->name = $request->name;
        if($request->text){
            $partner->tekst = $request->text;
        }

        if($request->file('image')){


            $filename = preg_replace('/[^A-Za-z0-9\-]/', '', str_replace(' ', '-', $request->name)); // Remove special chars & replace spaces with dashes

            $file = $request->file('image');
            $rules = array('file' => 'required|mimes:png,gif,jpeg', 'file' => 'max:3200');
            $validator = Validator::make(array('file'=> $file), $rules);
            if($validator->passes()){

              $filename = $filename."_".substr(md5(time()),0, 10)."_".$partner->id.".".$request->image->extension();
              $path = public_path("images/uploads/partners/".$filename);
              ImageProcessor::make($file)->resize(480, null, function ($constraint) {$constraint->aspectRatio();})->save($path);
              
              // save image & partner
              $partner->image = $filename;
              $partner->save();

               Session::flash("succes_message", "Partner <strong>$partner->name</strong> is bijgewerkt.");
               return Redirect::to("/partners/$partner->id/edit");

            }else{
                Session::flash("error_message", "Afbeelding moet een .jpg, .png of .gif zijn en mag maximaal 3MB groot zijn.");
                return Redirect::back()->withInput($request->input());
            }
        }else{
            $partner->save();

            Session::flash("succes_message", "Partner <strong>$partner->name</strong> is bijgewerkt.");
            return Redirect::to("/partners/$partner->id/edit");
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Partner  $partner
     * @return \Illuminate\Http\Response
     */
    public function destroy(Partner $partner)
    {
        //
    }
}
