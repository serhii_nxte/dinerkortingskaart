<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\User;
use App\Card;
use App\Partner;
use Auth;
use Redirect;
use Session;
use Hash;

class CardController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(request $request)
    {
        $cards = Card::orderBy('id');
        if($request->cardnumber){
            $cards->where('cardnumber', 'like', '%'.$request->cardnumber.'%');
        }
        if($request->partner){
            if($request->partner !== "-"){
                $cards->where('partner_id', $request->partner);
            }  
        }
        if($request->expiration){
            $cards->where('expiration', date('Y-m-d', strtotime($request->expiration)));
        }
        if($request->lname){
            $cards->with('user')->whereHas('user', function ($query) use ($request){
                $query->Where('lname', 'like', '%'.$request->lname.'%');
            });
        }
        if($request->email){
            $cards->with('user')->whereHas('user', function ($query) use ($request){
                $query->Where('email', 'like', '%'.$request->email.'%');
            });
        }

        $cards = $cards->paginate(12);
        $partners = Partner::all();
        return view('cards.index')
                    ->with('partners', $partners)
                    ->with('cards', $cards);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function filter(request $request)
    {
        $cards = Card::orderBy('id');
        
        // if($request->user){
        //     echo $request->user;
        // }
        $cards = $cards->paginate(12);       


        $partners = Partner::all();
        return view('cards.index')
                    ->with('partners', $partners)
                    ->with('cards', $cards);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $partners = Partner::all();
        $users = User::where('admin', 0)->get();
        return view('cards.create')
                    ->with('partners', $partners)
                    ->with('users', $users);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $card = new Card;
        $card->cardnumber = 0;
        $card->activation = 0;
        if(isset($request->activatie_geldig)){
            $card->period = $request->period;
        }else{
            $card->expiration = date('Y-m-d', strtotime($request->date));
        }
        if($request->partner !== "-"){
            $card->partner_id = $request->partner;
        }
        if($request->gebruiker !== "-"){
            $card->user_id = $request->gebruiker;
        }
        $card->save();

        $generateCardnumber = Auth::user()->generateCode($card->id);
        $card->cardnumber = $generateCardnumber;
        $card->activation = $generateCardnumber;
        $card->save();
        
        Session::flash("succes_message", "Kaart <strong>$card->cardnumber</strong> is aangemaakt.");
        return Redirect::to('/cards');

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Card  $card
     * @return \Illuminate\Http\Response
     */
    public function show(Card $card)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Card  $card
     * @return \Illuminate\Http\Response
     */
    public function edit(Card $card)
    {
        $partners = Partner::all();
        $users = User::where('admin', 0)->get();
        return view('cards.edit')
                    ->with('card', $card)
                    ->with('partners', $partners)
                    ->with('users', $users);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Card  $card
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Card $card)
    {
        if(isset($request->activatie_geldig)){
            $card->period = $request->period;
            $card->expiration = NULL;
        }else{
            $card->expiration = date('Y-m-d', strtotime($request->date));
        }
        if($request->partner !== "-"){
            $card->partner_id = $request->partner;
        }else{
            $card->partner_id = NULL;
        }
        if($request->gebruiker !== "-"){
            $card->user_id = $request->gebruiker;
        }else{
            $card->user_id = NULL;
        }
        $card->save();

        Session::flash("succes_message", "Kaart <strong>$card->cardnumber</strong> is bijgewerkt.");
        return Redirect::to('/cards');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function import()
    {
        $partners = Partner::all();
        return view('cards.import')->with('partners', $partners);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function importFunction(request $request)
    {

        $file = file($request->import);

        // CSV to array
        foreach($file as $k){
            $csv[] = explode(',', $k);
        }

        $i=0;
        $imported=0;
        $ignored=0;
        foreach ($csv as $value) {
            $i++;
            if($i!==1){
                if(!Card::where('cardnumber', $value[0])->count()){
                    echo "Importing card<br/>";
                    $card = new Card;
                    if($request->partner !== "-"){
                        $card->partner_id = $request->partner;
                    }
                    if($request->expiration == "date"){
                        $card->expiration = date('Y-m-d', strtotime(str_replace('/', '-', $value[1])));
                    }else{
                        $card->period = (int)$value[1];
                    }
                    $card->cardnumber = $value[0];
                    $card->activation = $value[0];
                    
                    $imported++;
                    $card->save();
                }else{
                    $ignored++;
                    echo "Card already exists<br/>";
                }
            }
        }

        Session::flash("succes_message", "$imported kaarten succesvol aangemaakt. $ignored regels genegeerd (kaartnummers bestaan al).");
        return Redirect::to('/cards');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Card  $card
     * @return \Illuminate\Http\Response
     */
    public function destroy(Card $card)
    {
        //
    }
}
