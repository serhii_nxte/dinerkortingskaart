<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use App\Card;
use App\User;
use App\Partner;
use Hash;
use Mail;
use App\Mail\passwordRecovery;

class AppController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function auth(request $request)
    { 
        if (Auth::attempt(['email' => $request->email, 'password' => $request->password])) {
            
            //check if user has valid card
            if(Auth::user()->cards->count()){

                // get card and partner data
                $card = Auth::user()->cards()->orderBy('expiration', 'desc')->first();
                $partnerImage = NULL;
                $partnertext = NULL;
                if(count($card->partner)){
                    $partnerImage = "https://dkk.mediacompanion.nl/images/uploads/partners/".$card->partner->image;
                    if($card->partner->tekst){
                        $partnertext = $card->partner->tekst;
                    }
                }

                // create the data array
                $dataArray = array(
                    "name" => Auth::user()->fname . " " . Auth::user()->lname,
                    "email" => Auth::user()->email,
                    "current" => "Kaart is geverifieerd op (".date('d-m-Y').")",
                    "expiration" => date('d-m-Y', strtotime($card->expiration)),
                    "card" => $card->cardnumber,
                    "partner_image" => $partnerImage,
                    "partner_text" => $partnertext,
                    "nocard" => NULL,
                    "msg" => NULL
                );

                if($card->isExpired()){
                    $dataArray['nocard'] = 1; // the card is expired
                    $dataArray['msg'] = "Uw vorige kaart (".$card->cardnumber.") is verlopen op: ".date('d-m-Y', strtotime($card->expiration));
                }

                // return array aas json
                return json_encode($dataArray);

            }else{
                // user has no card
                // create the data array
                $dataArray = array(
                    "name" => NULL,
                    "email" => $request->email,
                    "current" => NULL,
                    "expiration" => NULL,
                    "card" => NULL,
                    "partner_image" => NULL,
                    "partner_text" => NULL,
                    "nocard" => 1,
                    "msg" => "Uw account heeft nog geen kaart"
                );

                // return array aas json
                return json_encode($dataArray);
            }
            
        }else{
            // wrong username or password
            echo '503::Email of wachtwoord is incorrect.';
        }
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function register(request $request)
    {
        $curdate = date('Y-m-d');
        // check if card exists
        if(Card::where('activation', $request->activation)->where('expiration', '>=', $curdate)->whereNull('user_id')->count() || Card::where('activation', $request->activation)->whereNull('expiration')->whereNull('user_id')->count()){

            // check if email exists
            if(!User::where('email', $request->email)->count()){

                // check if email is valid
                if(filter_var($request->email, FILTER_VALIDATE_EMAIL)){

                    if($request->password == $request->repeat){
                        if(strlen($request->password) > 5){

                            // create user
                            $user = new User;
                            $user->fname = $request->fname;
                            $user->lname = $request->lname;
                            $user->email = $request->email;
                            $user->password = Hash::make($request->password);
                            $user->save();
                            
                            $card = Card::where('activation', $request->activation)->first();
                            if(is_null($card->expiration)){
                                $period = round($card->period * 30.5);
                                $card->expiration = date('Y-m-d', strtotime("+$period days"));
                            }
                            $card->user_id = $user->id;
                            $card->save();
                            
                            // get card and partner data
                            $partnerImage = NULL;
                            $partnertext = NULL;
                            if(count($card->partner)){
                                $partnerImage = "https://dkk.mediacompanion.nl/images/uploads/partners/".$card->partner->image;
                                if($card->partner->tekst){
                                    $partnertext = $card->partner->tekst;
                                }
                            }
                            
                            // create the data array
                            $dataArray = array(
                                "name" => $user->fname . " " . $user->lname,
                                "email" => $user->email,
                                "current" => "Kaart is geverifieerd op (".date('d-m-Y').")",
                                "expiration" => date('d-m-Y', strtotime($card->expiration)),
                                "card" => $card->cardnumber,
                                "partner_image" => $partnerImage,
                                "partner_text" => $partnertext
                            );

                            // return array aas json
                            return json_encode($dataArray);

                        }else{
                            echo '500::Wachtwoord moet minimaal 6 tekens lang zijn.';
                        }
                    }else{
                        echo '500::Wachtwoord en wachtwoord herhalen zijn niet gelijk.';
                    }
                }else{
                    echo '500::Ongeldig mailadres. Probeer een ander mailadres.';
                }

            }else{
                echo '500::Er bestaat al een gebruiker met dit e-mailadres.';
            }

        }else{
            echo '404::Kaartnummer / kaartcode is incorrect, al toegewezen aan een account of verlopen.';
        }

    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function addCard(request $request)
    {
        $curdate = date('Y-m-d');
        // check if card exists
        if(Card::where('activation', $request->activation)->where('expiration', '>=', $curdate)->whereNull('user_id')->count() || Card::where('activation', $request->activation)->whereNull('expiration')->whereNull('user_id')->count()){
            if (Auth::attempt(['email' => $request->email, 'password' => $request->password])) {

                $card = Card::where('activation', $request->activation)->first();
                $card->user_id = Auth::user()->id;
                if(is_null($card->expiration)){
                    $period = round($card->period * 30.5);
                    $card->expiration = date('Y-m-d', strtotime("+$period days"));
                }
                $card->save();

                // get card and partner data
                $partnerImage = NULL;
                $partnertext = NULL;
                if(count($card->partner)){
                    $partnerImage = "https://dkk.mediacompanion.nl/images/uploads/partners/".$card->partner->image;
                    if($card->partner->tekst){
                        $partnertext = $card->partner->tekst;
                    }
                }

                // create the data array
                $dataArray = array(
                    "name" => Auth::user()->fname . " " . Auth::user()->lname,
                    "email" => Auth::user()->email,
                    "current" => "Kaart is geverifieerd op (".date('d-m-Y').")",
                    "expiration" => date('d-m-Y', strtotime($card->expiration)),
                    "card" => $card->cardnumber,
                    "partner_image" => $partnerImage,
                    "partner_text" => $partnertext
                );

                // return array aas json
                return json_encode($dataArray);                

            }else{
                // wrong username or password
                echo '503::Email of wachtwoord is incorrect, Ga terug en probeer opnieuw'; 
            }
        }else{
            echo '404::Kaartnummer / kaartcode is incorrect, al toegewezen aan een account of verlopen.';
        }
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function fbAuth(request $request)
    {

        $access_token = $request->fbtoken;
        $user_details = "https://graph.facebook.com/me?fields=email,first_name,last_name&access_token=".$access_token;
        $response = file_get_contents($user_details);
        $response = json_decode($response);

        if(!empty($response->email)){
            $user = User::where('email', $response->email)->first();
            if(Auth::loginUsingId($user->id)){
                
                //check if user has valid card
                if(Auth::user()->cards->count()){

                    // HAS CARD
                    // get card and partner data
                    $card = Auth::user()->cards()->orderBy('expiration', 'desc')->first();
                    $partnerImage = NULL;
                    $partnertext = NULL;
                    if(count($card->partner)){
                        $partnerImage = "https://dkk.mediacompanion.nl/images/uploads/partners/".$card->partner->image;
                        if($card->partner->tekst){
                            $partnertext = $card->partner->tekst;
                        }
                    }

                    // create the data array
                    $dataArray = array(
                        "name" => Auth::user()->fname . " " . Auth::user()->lname,
                        "email" => Auth::user()->email,
                        "current" => "Kaart is geverifieerd op (".date('d-m-Y').")",
                        "expiration" => date('d-m-Y', strtotime($card->expiration)),
                        "card" => $card->cardnumber,
                        "partner_image" => $partnerImage,
                        "partner_text" => $partnertext,
                        "nocard" => NULL,
                        "msg" => NULL
                    );

                    if($card->isExpired()){
                        $dataArray['nocard'] = 1; // the card is expired
                        $dataArray['msg'] = "Uw vorige kaart (".$card->cardnumber.") is verlopen op: ".date('d-m-Y', strtotime($card->expiration));
                    }

                    // return array aas json
                    return json_encode($dataArray);

                }else{

                    // NO CARD
                    // create the data array
                    $dataArray = array(
                        "name" => NULL,
                        "email" => $request->email,
                        "current" => NULL,
                        "expiration" => NULL,
                        "card" => NULL,
                        "partner_image" => NULL,
                        "partner_text" => NULL,
                        "nocard" => 1,
                        "msg" => "Uw account heeft nog geen kaart"
                    );

                    // return array aas json
                    return json_encode($dataArray);
                }

            }else{

                // GENERATE RANDOM PASSWORD
                $chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789!@#$%^&*()_-=+;:,.?";
                $password = substr( str_shuffle( $chars ), 0, 12 );

                // CREATE ACCOUNT
                $user = new User;
                $user->fname = $response->fname;
                $user->lname = $response->lname;
                $user->email = $response->email;
                $user->password = Hash::make($password);
                $user->save();
                
                // create the data array
                $dataArray = array(
                    "name" => NULL,
                    "email" => $response->email,
                    "current" => NULL,
                    "expiration" => NULL,
                    "card" => NULL,
                    "partner_image" => NULL,
                    "partner_text" => NULL,
                    "nocard" => 1,
                    "msg" => "Uw account heeft nog geen kaart"
                );

                // return array aas json
                return json_encode($dataArray);

            }
        }else{
            // EMAIL COULD NOT BE RETRIEVED FROM FACEBOOK
            echo '500::Verficatie met facebook mislukt, probeer het opnieuw of log in met e-mail en wachtwoord.'; 
        }
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function forgotPassword(request $request)
    {
        $email = $request->email;
        $user = User::where('email', '=', $email)->first();
        if ($user !== null) {
            $recovery = substr(md5(mt_rand()), 0, 7);
            $expire = strtotime("+1 day");
            $expire = date('Y-m-d H:i:s', $expire);

            $user->password_recovery = $recovery;
            $user->password_recovery_expire = $expire;
            $user->save();

            Mail::to($email)->send(new passwordRecovery($user));

            return $email;
        }else{
            echo '500::Er is geen gebruiker met het opgegeven e-mailadres gevonden.'; 
        }
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function newPassword(request $request)
    {

        if($request->password == $request->passwordrepeat){

            $email = $request->email;
            $recovery = strtolower($request->recovery);
            $password = strtolower($request->password);

            $user = User::where('email', '=', $email)->first();
            if ($user !== null) {
                if($user->password_recovery == $recovery && strtotime(Date('Y-m-d H:i:s')) <= strtotime($user->password_recovery_expire)){

                    // change password
                    $user->password = Hash::make($password);
                    $user->password_recovery = NULL;
                    $user->password_recovery_expire = NULL;
                    $user->save();

                    if($user->cards->count()){

                        // get card and partner data
                        $card = $user->cards()->orderBy('expiration', 'desc')->first();
                        $partnerImage = NULL;
                        $partnertext = NULL;
                        if(count($card->partner)){
                            $partnerImage = "https://dkk.mediacompanion.nl/images/uploads/partners/".$card->partner->image;
                            if($card->partner->tekst){
                                $partnertext = $card->partner->tekst;
                            }
                        }

                        // create the data array
                        $dataArray = array(
                            "name" => $user->fname . " " . $user->lname,
                            "email" => $user->email,
                            "current" => "Kaart is geverifieerd op (".date('d-m-Y').")",
                            "expiration" => date('d-m-Y', strtotime($card->expiration)),
                            "card" => $card->cardnumber,
                            "partner_image" => $partnerImage,
                            "partner_text" => $partnertext,
                            "nocard" => NULL,
                            "msg" => NULL
                        );

                        if($card->isExpired()){
                            $dataArray['nocard'] = 1; // the card is expired
                            $dataArray['msg'] = "Uw vorige kaart (".$card->cardnumber.") is verlopen op: ".date('d-m-Y', strtotime($card->expiration));
                        }

                        // return array aas json
                        return json_encode($dataArray);

                    }else{
                        // user has no card
                        // create the data array
                        $dataArray = array(
                            "name" => NULL,
                            "email" => $request->email,
                            "current" => NULL,
                            "expiration" => NULL,
                            "card" => NULL,
                            "partner_image" => NULL,
                            "partner_text" => NULL,
                            "nocard" => 1,
                            "msg" => "Uw account heeft nog geen kaart"
                        );

                        // return array aas json
                        return json_encode($dataArray);
                    }

                }else{
                    echo '500::Code is incorrect of niet langer geldig. Probeer het opnieuw.';
                }
            }else{
                echo '500::Er is geen gebruiker met het opgegeven e-mailadres gevonden.'; 
            }

        }else{
            echo '500::Wachtwoord en wachtwoord herhalen zijn niet gelijk.';
        }
        

        
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
