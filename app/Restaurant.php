<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Restaurant extends Model
{
	/**
     * Get the kitchen
     */
    public function kitchen()
    {
        return $this->belongsTo('App\Kitchen');
    }

    // get day string
    public function days()
    {
    	$days = "";
    	if(!is_null($this->lunch_from_1) || !is_null($this->diner_from_1)){
    		$days .= "Ma, ";
    	}
    	if(!is_null($this->lunch_from_2) || !is_null($this->diner_from_2)){
    		$days .= "Di, ";
    	}
    	if(!is_null($this->lunch_from_3) || !is_null($this->diner_from_3)){
    		$days .= "Wo, ";
    	}
    	if(!is_null($this->lunch_from_4) || !is_null($this->diner_from_4)){
    		$days .= "Do, ";
    	}
    	if(!is_null($this->lunch_from_5) || !is_null($this->diner_from_5)){
    		$days .= "Vr, ";
    	}
    	if(!is_null($this->lunch_from_6) || !is_null($this->diner_from_6)){
    		$days .= "Za, ";
    	}
    	if(!is_null($this->lunch_from_7) || !is_null($this->diner_from_7)){
    		$days .= "Zo, ";
    	}
    	$days = substr($days, 0, -2);
    	if($days==""){
    		$days = null;
    	}
    	return $days;
    }
}
