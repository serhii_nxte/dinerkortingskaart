<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Partner extends Model
{
	/**
	 * Get the post that owns the comment.
	 */
    public function cards(){
        return $this->hasMany('App\Card');
    }
}
