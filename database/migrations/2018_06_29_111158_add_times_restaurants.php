<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddTimesRestaurants extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('restaurants', function (Blueprint $table) {
            $table->string('lunch_from_1')->nullable();
            $table->string('lunch_to_1')->nullable();
            $table->string('lunch_from_2')->nullable();
            $table->string('lunch_to_2')->nullable();
            $table->string('lunch_from_3')->nullable();
            $table->string('lunch_to_3')->nullable();
            $table->string('lunch_from_4')->nullable();
            $table->string('lunch_to_4')->nullable();
            $table->string('lunch_from_5')->nullable();
            $table->string('lunch_to_5')->nullable();
            $table->string('lunch_from_6')->nullable();
            $table->string('lunch_to_6')->nullable();
            $table->string('lunch_from_7')->nullable();
            $table->string('lunch_to_7')->nullable();
            $table->string('diner_from_1')->nullable();
            $table->string('diner_to_1')->nullable();
            $table->string('diner_from_2')->nullable();
            $table->string('diner_to_2')->nullable();
            $table->string('diner_from_3')->nullable();
            $table->string('diner_to_3')->nullable();
            $table->string('diner_from_4')->nullable();
            $table->string('diner_to_4')->nullable();
            $table->string('diner_from_5')->nullable();
            $table->string('diner_to_5')->nullable();
            $table->string('diner_from_6')->nullable();
            $table->string('diner_to_6')->nullable();
            $table->string('diner_from_7')->nullable();
            $table->string('diner_to_7')->nullable();
            $table->integer('kitchen_id')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('restaurants', function (Blueprint $table) {
            $table->dropColumn('lunch_from_1');
            $table->dropColumn('lunch_to_1');
            $table->dropColumn('lunch_from_2');
            $table->dropColumn('lunch_to_2');
            $table->dropColumn('lunch_from_3');
            $table->dropColumn('lunch_to_3');
            $table->dropColumn('lunch_from_4');
            $table->dropColumn('lunch_to_4');
            $table->dropColumn('lunch_from_5');
            $table->dropColumn('lunch_to_5');
            $table->dropColumn('lunch_from_6');
            $table->dropColumn('lunch_to_6');
            $table->dropColumn('lunch_from_7');
            $table->dropColumn('lunch_to_7');
            $table->dropColumn('diner_from_1');
            $table->dropColumn('diner_to_1');
            $table->dropColumn('diner_from_2');
            $table->dropColumn('diner_to_2');
            $table->dropColumn('diner_from_3');
            $table->dropColumn('diner_to_3');
            $table->dropColumn('diner_from_4');
            $table->dropColumn('diner_to_4');
            $table->dropColumn('diner_from_5');
            $table->dropColumn('diner_to_5');
            $table->dropColumn('diner_from_6');
            $table->dropColumn('diner_to_6');
            $table->dropColumn('diner_from_7');
            $table->dropColumn('diner_to_7');
            $table->dropColumn('kitchen_id');
        });
    }
}
