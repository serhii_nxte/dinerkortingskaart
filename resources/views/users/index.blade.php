@extends('layouts.app')

@section('pageTitle')
    Gebruikers
@endsection

@section('pageActions')
	<div class="col-md-12">
		<p class="pull-left record-counter">{{$users->count()}} Records found</p>
		
		<a href="/users/create" class="btn btn-primary pull-right"><i class="fa fa-plus" aria-hidden="true"></i> Aanmaken</a>
		{{-- <a href="/users/import" class="btn btn-primary pull-right m-r-h"><i class="fa fa-download" aria-hidden="true"></i> Importeren</a> --}}
	</div>
@endsection

@section('content')
    <div class="col-md-12">
    	{!! Form::open(['method' => 'GET']) !!}
        <table class="table ftable table-striped footable">
        	
        	<thead>
	        	<tr>
	        		<th class="footable-sortable">Id</th>
	        		<th class="footable-sortable">
	        			Naam<br/>
	        			<input type="text" class="form-control" name="naam" @if(isset($_GET['naam'])) value="{{$_GET['naam']}}" @endif>
	        		</th>
	        		<th class="footable-sortable">
	        			E-mail<br/>
	        			<input type="text" class="form-control" name="email" @if(isset($_GET['email'])) value="{{$_GET['email']}}" @endif>
	        		</th>
	        		<th class="footable-sortable">Kaart(en)</th>
	        		<th><input type="submit" class="btn btn-primary pull-right" value="Zoeken"></th>
	        	</tr>
        	</thead>
        	
        	<tbody>
	        	@foreach($users as $user)
	        		<tr>
	        			<td>{{$user->id}}</td>
	        			<td>{{$user->fname}} {{$user->lname}}</td>
	        			<td>{{$user->email}}</td>
	        			<td>
	        				@if($user->cards->count())
	        					@foreach($user->cards()->get() as $card)
	        						@if($curDate < $card->expiration)
	        							{{$card->cardnumber}}<br/>
	        						@else
	        							{{$card->cardnumber}} (verlopen)
	        						@endif
	        					@endforeach
	        				@else
	        					-
	        				@endif
	        			</td>
	        			<td>
	        				<a class="btn btn-primary pull-right" href="/users/{{$user->id}}/edit"><i class="fa fa-pencil"></i></a>
	        			</td>
	        		</tr>
	        	@endforeach
	        </tbody>
	        <tfoot>
	            <tr>
                    <td colspan="6">
                        <ul class="pagination pull-right">
                        	{{$users->appends($_GET)->links()}}
                        </ul>
                    </td>
                </tr>
            </tfoot>
        </table>
        {!! Form::close() !!}
    </div>
@endsection

@section('scripts')
<script type="text/javascript">
	$(document).ready(function() {
		$('.ftable').footable();
	});
</script>
@append
