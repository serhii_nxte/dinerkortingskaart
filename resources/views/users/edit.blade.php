@extends('layouts.app')

@section('pageTitle')
    Gebruiker aanpassen
@endsection

@section('pageActions')
	<div class="col-md-12">
		<a href="#" class="btn btn-primary form-save pull-right">Opslaan</a>
        <a class="pull-right m-r-h m-t-q" href="/users">< Terug</a>
	</div>
@endsection

@section('content')

    <div class="col-md-12">
        {!! Form::open(["url" => "/users/$user->id", "id"=>"submit-form", "method"=>"PATCH"]) !!}

            <div class="fw">
                <div class="col-md-2 no-p-l pull-left">
                    <label class="m-t-h m-b-h" for="name">Voornaam</label>
                </div>
                <div class="col-md-10 form-border-left no-p-r pull-right">
                    <input id="name" type="text" name="fname" class="form-control m-t-h m-b-h" value="{{ $user->fname }}" required>
                </div>
            </div>

            <div class="fw">
                <div class="col-md-2 no-p-l pull-left">
                    <label class="m-t-h m-b-h" for="name">Achternaam</label>
                </div>
                <div class="col-md-10 form-border-left no-p-r pull-right">
                    <input id="name" type="text" name="lname" class="form-control m-t-h m-b-h" value="{{ $user->lname }}" required>
                </div>
            </div>

            <div class="fw">
                <div class="col-md-2 no-p-l pull-left">
                    <label class="m-t-h m-b-h" for="name">E-mail</label>
                </div>
                <div class="col-md-10 form-border-left no-p-r pull-right">
                    <input id="name" type="text" name="email" class="form-control m-t-h m-b-h" value="{{ $user->email }}" required>
                </div>
            </div>

            <div class="fw">
                <div class="col-md-2 no-p-l pull-left">
                    <label class="m-t-h m-b-h" for="name">Wachtwoord</label>
                </div>
                <div class="col-md-10 form-border-left no-p-r pull-right">
                    <input id="name" type="password" name="password" class="form-control m-t-h m-b-h">
                    <small class="help-block bg-info">Laat leeg als het wachtwoord niet veranderd dient te worden!</small>
                </div>
            </div>

            <div class="fw">
                <div class="col-md-2 no-p-l pull-left">
                    <label class="m-t-h m-b-h" for="name">Wachtwoord herhalen</label>
                </div>
                <div class="col-md-10 form-border-left no-p-r pull-right">
                    <input id="name" type="password" name="passwordrepeat" class="form-control m-t-h m-b-h">
                    <small class="help-block bg-info">Laat leeg als het wachtwoord niet veranderd dient te worden!</small>
                </div>
            </div>

            <div class="fw">
                <div class="col-md-2 no-p-l pull-left">
                    <label class="m-t-h m-b-h" for="name">Activatiecode / kaartnummer</label>
                </div>
                <div class="col-md-10 form-border-left no-p-r pull-right">
                    <input id="name" type="text" name="activationcode" class="form-control m-t-h m-b-h">
                    <small class="help-block bg-info">Laat leeg als er geen nieuwe kaart gekoppeld dient te worden!</small>
                </div>
            </div>

            <div class="fw">
                <button class="btn btn-primary pull-right" type="submit">Opslaan</button>
            </div>

		{!! Form::close() !!}
    </div>
@endsection

@section('scripts')
    <script type="text/javascript">
        $( document ).ready(function() {

            $(".chzn-select").chosen();

            // course picker
            $('input[type=radio][name=type]').on('change', function() {
                 switch($(this).val()) {
                     case '1':
                        $('.food-group').addClass('active');
                        $('.drink-group').removeClass('active');
                        $('.food-group').show();
                        $('.drink-group').hide();
                        $('.no-option').hide();
                        $("input[name='course']").val([]);
                        break;
                     case '0':
                        $('.food-group').removeClass('active');
                        $('.drink-group').addClass('active');
                        $('.food-group').hide();
                        $('.drink-group').show();
                        $('.no-option').hide();
                        $("input[name='course']").val([]);
                        break;
                 }
            });

            $(".form-save").click(function() {
                $('#submit-form').find('[type="submit"]').trigger('click');
            });
        });
    </script>
@append