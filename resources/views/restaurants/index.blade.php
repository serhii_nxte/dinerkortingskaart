@extends('layouts.app')

@section('pageTitle')
    Restaurants
@endsection

@section('pageActions')
	<div class="col-md-12">
		<p class="pull-left record-counter">{{$restaurants->count()}} Records found</p>
		
		<a href="/restaurants/create" class="btn btn-primary pull-right"><i class="fa fa-plus" aria-hidden="true"></i> Aanmaken</a>
		<a href="/restaurants/import" class="btn btn-primary pull-right m-r-h"><i class="fa fa-download" aria-hidden="true"></i> Importeren</a>
	</div>
@endsection

@section('content')
    <div class="col-md-12">
    	{!! Form::open(['method' => 'GET']) !!}
        <table class="table ftable table-striped footable">
        	
        	<thead>
	        	<tr>
	        		<th class="footable-sortable">Id</th>
	        		<th class="footable-sortable">Afbeelding</th>
	        		<th class="footable-sortable">
	        			Naam<br/>
	        			<input type="text" class="form-control" name="naam" @if(isset($_GET['naam'])) value="{{$_GET['naam']}}" @endif>
	        		</th>
	        		<th><input type="submit" class="btn btn-primary pull-right" value="Zoeken"></th>
	        	</tr>
        	</thead>
        	
        	<tbody>
	        	@foreach($restaurants as $restaurant)
					<tr>
						<td>{{$restaurant->id}}</td>
						<td>
							@if(!is_null($restaurant->image))
								@if(strpos($restaurant->image, ',') !== false)
									<?php $img = explode(',', $restaurant->image); $img = $img[0]; ?>
									<img style="max-width: 240px;" src="{{$img}}" alt="{{$restaurant->name}}">
								@else
									<img style="max-width: 240px;" src="{{$restaurant->image}}" alt="{{$restaurant->name}}">
								@endif
							@else
								<img style="max-width: 240px;" src="http://via.placeholder.com/580x200" alt="{{$restaurant->name}}">
							@endif
						</td>
						<td>{{$restaurant->name}} @if($restaurant->active == 0) <span class="badge small">NIET ACTIEF</span> @endif</td>
						<td>
							<a class="btn btn-primary pull-right" href="/restaurants/{{$restaurant->id}}/edit"><i class="fa fa-pencil"></i></a>
						</td>
					</tr>
	        	@endforeach
	        </tbody>
	        <tfoot>
	            <tr>
                    <td colspan="6">
                        <ul class="pagination pull-right">
                        	{{$restaurants->appends($_GET)->links()}}
                        </ul>
                    </td>
                </tr>
            </tfoot>
        </table>
        {!! Form::close() !!}
    </div>
@endsection

@section('scripts')
<script type="text/javascript">
	$(document).ready(function() {
		$('.ftable').footable();
	});
</script>
@append
