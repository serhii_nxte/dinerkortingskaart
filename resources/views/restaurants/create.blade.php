@extends('layouts.app')

@section('pageTitle')
    Nieuw restaurant
@endsection

@section('pageActions')
	<div class="col-md-12">
		<a href="#" class="btn btn-primary form-save pull-right">Opslaan</a>
        <a class="pull-right m-r-h m-t-q" href="/restaurants">< Terug</a>
	</div>
@endsection

@section('content')

    <div class="col-md-12">
        {!! Form::open(['url' => '/restaurants', "id"=>"submit-form", "files"=>true]) !!}
            
            <div class="fw">
                <div class="col-md-2 no-p-l pull-left">
                    <label class="m-t-h m-b-h" for="name">Actief*</label>
                </div>
                <div class="col-md-10 form-border-left no-p-r pull-right">
                    <select name="active" class="form-control">
                        <option value="0" >NIET ACTIEF</option>
                        <option value="1" selected>ACTIEF</option>
                    </select>
                </div>
            </div>

            <div class="fw">
                <div class="col-md-2 no-p-l pull-left">
                    <label class="m-t-h m-b-h" for="name">Naam*</label>
                </div>
                <div class="col-md-10 form-border-left no-p-r pull-right">
                    <input id="name" type="text" name="name" class="form-control m-t-h m-b-h" value="{{ old('name') }}" required>
                </div>
            </div>

            <div class="fw">
                <div class="col-md-2 no-p-l pull-left">
                    <label class="m-t-h m-b-h" for="name">Adres*</label>
                </div>
                <div class="col-md-10 form-border-left no-p-r pull-right">
                    <input id="name" type="text" name="address" class="form-control m-t-h m-b-h" value="{{ old('address') }}" required>
                </div>
            </div>

            <div class="fw">
                <div class="col-md-2 no-p-l pull-left">
                    <label class="m-t-h m-b-h" for="name">Postcode*</label>
                </div>
                <div class="col-md-10 form-border-left no-p-r pull-right">
                    <input id="name" type="text" name="zip" class="form-control m-t-h m-b-h" value="{{ old('zip') }}" required>
                </div>
            </div>

            <div class="fw">
                <div class="col-md-2 no-p-l pull-left">
                    <label class="m-t-h m-b-h" for="name">Stad*</label>
                </div>
                <div class="col-md-10 form-border-left no-p-r pull-right">
                    <input id="name" type="text" name="city" class="form-control m-t-h m-b-h" value="{{ old('city') }}" required>
                </div>
            </div>

            <div class="fw">
                <div class="col-md-2 no-p-l pull-left">
                    <label class="m-t-h m-b-h" for="name">Telefoon</label>
                </div>
                <div class="col-md-10 form-border-left no-p-r pull-right">
                    <input id="name" type="text" name="phone" class="form-control m-t-h m-b-h" value="{{ old('phone') }}">
                </div>
            </div>

            <div class="fw">
                <div class="col-md-2 no-p-l pull-left">
                    <label class="m-t-h m-b-h" for="name">Website</label>
                </div>
                <div class="col-md-10 form-border-left no-p-r pull-right">
                    <input id="name" type="text" name="website" class="form-control m-t-h m-b-h" value="{{ old('website') }}">
                </div>
            </div>

            <div class="fw">
                <div class="col-md-2 no-p-l pull-left">
                    <label class="m-t-h m-b-h" for="name">Omschrijving</label>
                </div>
                <div class="col-md-10 form-border-left no-p-r pull-right">
                    <textarea name="description" class="form-control m-t-h m-b-h">{{ old('description') }}</textarea>
                </div>
            </div>

            <div class="fw">
                <div class="col-md-2 no-p-l pull-left">
                    <label class="m-t-h m-b-h" for="name">Korting tekst</label>
                </div>
                <div class="col-md-10 form-border-left no-p-r pull-right">
                    <input id="name" type="text" name="discount" class="form-control m-t-h m-b-h" value="{{ old('discount') }}">
                </div>
            </div>

            <div class="fw">
                <div class="col-md-2 no-p-l pull-left">
                    <label class="m-t-h m-b-h" for="name">Keuken</label>
                </div>
                <div class="col-md-10 form-border-left no-p-r pull-right">
                    <select name="kitchen" class="form-control" required>
                        <option value="" selected disabled>Kies een keuken...</option>
                        @foreach($kitchens as $kitchen)
                            <option value="{{$kitchen->id}}">{{$kitchen->name}}</option>
                        @endforeach
                    </select>
                </div>
            </div>

            <div class="fw">
                <div class="col-md-2 no-p-l pull-left">
                    <label class="m-t-h m-b-h" for="name">Afbeelding</label>
                </div>
                <div class="col-md-10 form-border-left no-p-r pull-right">
                    <img id="image" class="fw m-b-h" style="max-width: 240px;" src="http://via.placeholder.com/580x200" alt="placeholder">
                    <input id="upload-image" type="file" name="image" class="form-control m-b-h">
                </div>
            </div>

            <div class="fw">
                <div class="col-md-2 no-p-l pull-left">
                    <label class="m-t-h m-b-h" for="name">Openingstijden</label>
                </div>
                <div class="col-md-10 form-border-left no-p-r pull-right">
                    <div class="col-md-6">
                        <p class="h3">Lunch</p>

                        <p class="fw m-t"><strong>Maandag</strong></p>
                        <div class="col-md-6 nopad">Van: <input type="time" class="form-control" style="max-width: 120px;" name="lunch_from_1"></div>
                        <div class="col-md-6 nopad">Tot:<input type="time" class="form-control" style="max-width: 120px;" name="lunch_to_1"></div>
    
                        <p class="fw m-t"><strong>Dinsdag</strong></p>
                        <div class="col-md-6 nopad">Van: <input type="time" class="form-control" style="max-width: 120px;" name="lunch_from_2"></div>
                        <div class="col-md-6 nopad">Tot:<input type="time" class="form-control" style="max-width: 120px;" name="lunch_to_2"></div>

                        <p class="fw m-t"><strong>Woensdag</strong></p>
                        <div class="col-md-6 nopad">Van: <input type="time" class="form-control" style="max-width: 120px;" name="lunch_from_3"></div>
                        <div class="col-md-6 nopad">Tot:<input type="time" class="form-control" style="max-width: 120px;" name="lunch_to_3"></div>

                        <p class="fw m-t"><strong>Donderdag</strong></p>
                        <div class="col-md-6 nopad">Van: <input type="time" class="form-control" style="max-width: 120px;" name="lunch_from_4"></div>
                        <div class="col-md-6 nopad">Tot:<input type="time" class="form-control" style="max-width: 120px;" name="lunch_to_4"></div>

                        <p class="fw m-t"><strong>Vrijdag</strong></p>
                        <div class="col-md-6 nopad">Van: <input type="time" class="form-control" style="max-width: 120px;" name="lunch_from_5"></div>
                        <div class="col-md-6 nopad">Tot:<input type="time" class="form-control" style="max-width: 120px;" name="lunch_to_5"></div>

                        <p class="fw m-t"><strong>Zaterdag</strong></p>
                        <div class="col-md-6 nopad">Van: <input type="time" class="form-control" style="max-width: 120px;" name="lunch_from_6"></div>
                        <div class="col-md-6 nopad">Tot:<input type="time" class="form-control" style="max-width: 120px;" name="lunch_to_6"></div>

                        <p class="fw m-t"><strong>Zondag</strong></p>
                        <div class="col-md-6 nopad">Van: <input type="time" class="form-control" style="max-width: 120px;" name="lunch_from_7"></div>
                        <div class="col-md-6 nopad">Tot:<input type="time" class="form-control" style="max-width: 120px;" name="lunch_to_7"></div>
                    </div>
                    <div class="col-md-6">
                        <p class="h3">Diner</p>

                        <p class="fw m-t"><strong>Maandag</strong></p>
                        <div class="col-md-6 nopad">Van: <input type="time" class="form-control" style="max-width: 120px;" name="diner_from_1"></div>
                        <div class="col-md-6 nopad">Tot:<input type="time" class="form-control" style="max-width: 120px;" name="diner_to_1"></div>
    
                        <p class="fw m-t"><strong>Dinsdag</strong></p>
                        <div class="col-md-6 nopad">Van: <input type="time" class="form-control" style="max-width: 120px;" name="diner_from_2"></div>
                        <div class="col-md-6 nopad">Tot:<input type="time" class="form-control" style="max-width: 120px;" name="diner_to_2"></div>

                        <p class="fw m-t"><strong>Woensdag</strong></p>
                        <div class="col-md-6 nopad">Van: <input type="time" class="form-control" style="max-width: 120px;" name="diner_from_3"></div>
                        <div class="col-md-6 nopad">Tot:<input type="time" class="form-control" style="max-width: 120px;" name="diner_to_3"></div>

                        <p class="fw m-t"><strong>Donderdag</strong></p>
                        <div class="col-md-6 nopad">Van: <input type="time" class="form-control" style="max-width: 120px;" name="diner_from_4"></div>
                        <div class="col-md-6 nopad">Tot:<input type="time" class="form-control" style="max-width: 120px;" name="diner_to_4"></div>

                        <p class="fw m-t"><strong>Vrijdag</strong></p>
                        <div class="col-md-6 nopad">Van: <input type="time" class="form-control" style="max-width: 120px;" name="diner_from_5"></div>
                        <div class="col-md-6 nopad">Tot:<input type="time" class="form-control" style="max-width: 120px;" name="diner_to_5"></div>

                        <p class="fw m-t"><strong>Zaterdag</strong></p>
                        <div class="col-md-6 nopad">Van: <input type="time" class="form-control" style="max-width: 120px;" name="diner_from_6"></div>
                        <div class="col-md-6 nopad">Tot:<input type="time" class="form-control" style="max-width: 120px;" name="diner_to_6"></div>

                        <p class="fw m-t"><strong>Zondag</strong></p>
                        <div class="col-md-6 nopad">Van: <input type="time" class="form-control" style="max-width: 120px;" name="diner_from_7"></div>
                        <div class="col-md-6 nopad">Tot:<input type="time" class="form-control" style="max-width: 120px;" name="diner_to_7"></div>
                    </div>
                </div>
            </div>

            <div class="fw">
                <button class="btn btn-primary pull-right m-t" type="submit">Opslaan</button>
            </div>

		{!! Form::close() !!}
    </div>
@endsection

@section('scripts')
<style>
    .nopad{
        padding: 0;
    }
</style>
    <script type="text/javascript">
        $( document ).ready(function() {

            function readURL(input) {

                if (input.files && input.files[0]) {
                    var reader = new FileReader();

                    reader.onload = function (e) {
                        $('#image').attr('src', e.target.result);
                    }

                    reader.readAsDataURL(input.files[0]);
                }
            }

            $("#upload-image").change(function(){
                readURL(this);
            });

            $(".form-save").click(function() {
                $('#submit-form').find('[type="submit"]').trigger('click');
            });
        });
    </script>
@append