@extends('layouts.app')

@section('pageTitle')
    Restaurant aanpassen
@endsection

@section('pageActions')
	<div class="col-md-12">
		<a href="#" class="btn btn-primary form-save pull-right">Opslaan</a>
        <a class="pull-right m-r-h m-t-q" href="/restaurants">< Terug</a>
	</div>
@endsection

@section('content')

    <div class="col-md-12">
        {!! Form::open(["url" => "/restaurants/$restaurant->id", "id"=>"submit-form", "method"=>"PATCH", "files"=>true]) !!}
            
            <div class="fw">
                <div class="col-md-2 no-p-l pull-left">
                    <label class="m-t-h m-b-h" for="name">Actief*</label>
                </div>
                <div class="col-md-10 form-border-left no-p-r pull-right">
                    <select name="active" class="form-control">
                        <option value="0" @if($restaurant->active == 0) selected @endif>NIET ACTIEF</option>
                        <option value="1" @if($restaurant->active == 1) selected @endif>ACTIEF</option>
                    </select>
                </div>
            </div>

            <div class="fw">
                <div class="col-md-2 no-p-l pull-left">
                    <label class="m-t-h m-b-h" for="name">Naam*</label>
                </div>
                <div class="col-md-10 form-border-left no-p-r pull-right">
                    <input id="name" type="text" name="name" class="form-control m-t-h m-b-h" value="{{ $restaurant->name }}" required>
                </div>
            </div>

            <div class="fw">
                <div class="col-md-2 no-p-l pull-left">
                    <label class="m-t-h m-b-h" for="name">Adres*</label>
                </div>
                <div class="col-md-10 form-border-left no-p-r pull-right">
                    <input id="name" type="text" name="address" class="form-control m-t-h m-b-h" value="{{ $restaurant->address }}" required>
                </div>
            </div>

            <div class="fw">
                <div class="col-md-2 no-p-l pull-left">
                    <label class="m-t-h m-b-h" for="name">Postcode*</label>
                </div>
                <div class="col-md-10 form-border-left no-p-r pull-right">
                    <input id="name" type="text" name="zip" class="form-control m-t-h m-b-h" value="{{ $restaurant->zip }}" required>
                </div>
            </div>

            <div class="fw">
                <div class="col-md-2 no-p-l pull-left">
                    <label class="m-t-h m-b-h" for="name">Stad*</label>
                </div>
                <div class="col-md-10 form-border-left no-p-r pull-right">
                    <input id="name" type="text" name="city" class="form-control m-t-h m-b-h" value="{{ $restaurant->city }}" required>
                </div>
            </div>

            <div class="fw">
                <div class="col-md-2 no-p-l pull-left">
                    <label class="m-t-h m-b-h" for="name">Telefoon</label>
                </div>
                <div class="col-md-10 form-border-left no-p-r pull-right">
                    <input id="name" type="text" name="phone" class="form-control m-t-h m-b-h" value="{{ $restaurant->phone }}">
                </div>
            </div>

            <div class="fw">
                <div class="col-md-2 no-p-l pull-left">
                    <label class="m-t-h m-b-h" for="name">Website</label>
                </div>
                <div class="col-md-10 form-border-left no-p-r pull-right">
                    <input id="name" type="text" name="website" class="form-control m-t-h m-b-h" value="{{ $restaurant->website }}">
                </div>
            </div>

            <div class="fw">
                <div class="col-md-2 no-p-l pull-left">
                    <label class="m-t-h m-b-h" for="name">Omschrijving</label>
                </div>
                <div class="col-md-10 form-border-left no-p-r pull-right">
                    <textarea name="description" class="form-control m-t-h m-b-h">{{ $restaurant->description }}</textarea>
                </div>
            </div>

            <div class="fw">
                <div class="col-md-2 no-p-l pull-left">
                    <label class="m-t-h m-b-h" for="name">Korting tekst</label>
                </div>
                <div class="col-md-10 form-border-left no-p-r pull-right">
                    <input id="name" type="text" name="discount" class="form-control m-t-h m-b-h" value="{{ $restaurant->discount }}">
                </div>
            </div>

            <div class="fw">
                <div class="col-md-2 no-p-l pull-left">
                    <label class="m-t-h m-b-h" for="name">Keuken</label>
                </div>
                <div class="col-md-10 form-border-left no-p-r pull-right">
                    <select name="kitchen" class="form-control" required>
                        <option value="" disabled>Kies een keuken...</option>
                        @foreach($kitchens as $kitchen)
                            <option @if($kitchen->id == $restaurant->kitchen_id) selected @endif value="{{$kitchen->id}}">{{$kitchen->name}}</option>
                        @endforeach
                    </select>
                </div>
            </div>

            <div class="fw">
                <div class="col-md-2 no-p-l pull-left">
                    <label class="m-t-h m-b-h" for="name">Afbeelding</label>
                </div>
                <div class="col-md-10 form-border-left no-p-r pull-right">
                    @if(!is_null($restaurant->image))
                        @if(strpos($restaurant->image, ',') !== false)
                            <?php $img = explode(',', $restaurant->image); $img = $img[0]; ?>
                            <img style="max-width: 240px;" src="{{$img}}" alt="{{$restaurant->name}}">
                        @else
                            <img style="max-width: 240px;" src="{{$restaurant->image}}" alt="{{$restaurant->name}}">
                        @endif
                    @else
                        <img id="image" class="fw m-b-h" style="max-width: 240px;" src="http://via.placeholder.com/580x200" alt="placeholder">
                    @endif
                    <input id="upload-image" type="file" name="image" class="form-control m-b-h">
                </div>
            </div>

            <div class="fw">
                <div class="col-md-2 no-p-l pull-left">
                    <label class="m-t-h m-b-h" for="name">Openingstijden</label>
                </div>
                <div class="col-md-10 form-border-left no-p-r pull-right">
                    <div class="col-md-6">
                        <p class="h3">Lunch</p>

                        <p class="fw m-t"><strong>Maandag</strong></p>
                        <div class="col-md-6 nopad">Van: <input type="time" class="form-control" style="max-width: 120px;" value="{{$restaurant->lunch_from_1}}" name="lunch_from_1"></div>
                        <div class="col-md-6 nopad">Tot:<input type="time" class="form-control" style="max-width: 120px;" value="{{$restaurant->lunch_to_1}}" name="lunch_to_1"></div>
    
                        <p class="fw m-t"><strong>Dinsdag</strong></p>
                        <div class="col-md-6 nopad">Van: <input type="time" class="form-control" style="max-width: 120px;" value="{{$restaurant->lunch_from_2}}" name="lunch_from_2"></div>
                        <div class="col-md-6 nopad">Tot:<input type="time" class="form-control" style="max-width: 120px;" value="{{$restaurant->lunch_to_2}}" name="lunch_to_2"></div>

                        <p class="fw m-t"><strong>Woensdag</strong></p>
                        <div class="col-md-6 nopad">Van: <input type="time" class="form-control" style="max-width: 120px;" value="{{$restaurant->lunch_from_3}}" name="lunch_from_3"></div>
                        <div class="col-md-6 nopad">Tot:<input type="time" class="form-control" style="max-width: 120px;" value="{{$restaurant->lunch_to_3}}" name="lunch_to_3"></div>

                        <p class="fw m-t"><strong>Donderdag</strong></p>
                        <div class="col-md-6 nopad">Van: <input type="time" class="form-control" style="max-width: 120px;" value="{{$restaurant->lunch_from_4}}" name="lunch_from_4"></div>
                        <div class="col-md-6 nopad">Tot:<input type="time" class="form-control" style="max-width: 120px;" value="{{$restaurant->lunch_to_4}}" name="lunch_to_4"></div>

                        <p class="fw m-t"><strong>Vrijdag</strong></p>
                        <div class="col-md-6 nopad">Van: <input type="time" class="form-control" style="max-width: 120px;" value="{{$restaurant->lunch_from_5}}" name="lunch_from_5"></div>
                        <div class="col-md-6 nopad">Tot:<input type="time" class="form-control" style="max-width: 120px;" value="{{$restaurant->lunch_to_5}}" name="lunch_to_5"></div>

                        <p class="fw m-t"><strong>Zaterdag</strong></p>
                        <div class="col-md-6 nopad">Van: <input type="time" class="form-control" style="max-width: 120px;" value="{{$restaurant->lunch_from_6}}" name="lunch_from_6"></div>
                        <div class="col-md-6 nopad">Tot:<input type="time" class="form-control" style="max-width: 120px;" value="{{$restaurant->lunch_to_6}}" name="lunch_to_6"></div>

                        <p class="fw m-t"><strong>Zondag</strong></p>
                        <div class="col-md-6 nopad">Van: <input type="time" class="form-control" style="max-width: 120px;" value="{{$restaurant->lunch_from_7}}" name="lunch_from_7"></div>
                        <div class="col-md-6 nopad">Tot:<input type="time" class="form-control" style="max-width: 120px;" value="{{$restaurant->lunch_to_7}}" name="lunch_to_7"></div>
                    </div>
                    <div class="col-md-6">
                        <p class="h3">Diner</p>

                        <p class="fw m-t"><strong>Maandag</strong></p>
                        <div class="col-md-6 nopad">Van: <input type="time" class="form-control" style="max-width: 120px;" value="{{$restaurant->diner_from_1}}" name="diner_from_1"></div>
                        <div class="col-md-6 nopad">Tot:<input type="time" class="form-control" style="max-width: 120px;" value="{{$restaurant->diner_to_1}}" name="diner_to_1"></div>
    
                        <p class="fw m-t"><strong>Dinsdag</strong></p>
                        <div class="col-md-6 nopad">Van: <input type="time" class="form-control" style="max-width: 120px;" value="{{$restaurant->diner_from_2}}" name="diner_from_2"></div>
                        <div class="col-md-6 nopad">Tot:<input type="time" class="form-control" style="max-width: 120px;" value="{{$restaurant->diner_to_2}}" name="diner_to_2"></div>

                        <p class="fw m-t"><strong>Woensdag</strong></p>
                        <div class="col-md-6 nopad">Van: <input type="time" class="form-control" style="max-width: 120px;" value="{{$restaurant->diner_from_3}}" name="diner_from_3"></div>
                        <div class="col-md-6 nopad">Tot:<input type="time" class="form-control" style="max-width: 120px;" value="{{$restaurant->diner_to_3}}" name="diner_to_3"></div>

                        <p class="fw m-t"><strong>Donderdag</strong></p>
                        <div class="col-md-6 nopad">Van: <input type="time" class="form-control" style="max-width: 120px;" value="{{$restaurant->diner_from_4}}" name="diner_from_4"></div>
                        <div class="col-md-6 nopad">Tot:<input type="time" class="form-control" style="max-width: 120px;" value="{{$restaurant->diner_to_4}}" name="diner_to_4"></div>

                        <p class="fw m-t"><strong>Vrijdag</strong></p>
                        <div class="col-md-6 nopad">Van: <input type="time" class="form-control" style="max-width: 120px;" value="{{$restaurant->diner_from_5}}" name="diner_from_5"></div>
                        <div class="col-md-6 nopad">Tot:<input type="time" class="form-control" style="max-width: 120px;" value="{{$restaurant->diner_to_5}}" name="diner_to_5"></div>

                        <p class="fw m-t"><strong>Zaterdag</strong></p>
                        <div class="col-md-6 nopad">Van: <input type="time" class="form-control" style="max-width: 120px;" value="{{$restaurant->diner_from_6}}" name="diner_from_6"></div>
                        <div class="col-md-6 nopad">Tot:<input type="time" class="form-control" style="max-width: 120px;" value="{{$restaurant->diner_to_6}}" name="diner_to_6"></div>

                        <p class="fw m-t"><strong>Zondag</strong></p>
                        <div class="col-md-6 nopad">Van: <input type="time" class="form-control" style="max-width: 120px;" value="{{$restaurant->diner_from_7}}" name="diner_from_7"></div>
                        <div class="col-md-6 nopad">Tot:<input type="time" class="form-control" style="max-width: 120px;" value="{{$restaurant->diner_to_7}}" name="diner_to_7"></div>
                    </div>
                </div>
            </div>

            <div class="fw">
                <button class="btn btn-primary pull-right m-t" type="submit">Opslaan</button>
            </div>

		{!! Form::close() !!}
    </div>
@endsection

@section('scripts')
    <script type="text/javascript">
        $( document ).ready(function() {

            function readURL(input) {

                if (input.files && input.files[0]) {
                    var reader = new FileReader();

                    reader.onload = function (e) {
                        $('#image').attr('src', e.target.result);
                    }

                    reader.readAsDataURL(input.files[0]);
                }
            }

            $("#upload-image").change(function(){
                readURL(this);
            });

            $(".form-save").click(function() {
                $('#submit-form').find('[type="submit"]').trigger('click');
            });
        });
    </script>
@append