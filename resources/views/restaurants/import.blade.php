@extends('layouts.app')

@section('pageTitle')
    Restaurants importeren
@endsection

@section('pageActions')
	<div class="col-md-12">
		<a href="#" class="btn btn-primary form-save pull-right">Opslaan</a>
        <a class="pull-right m-r-h m-t-q" href="/restaurants">< Naar restaurant overzicht</a>
	</div>
@endsection

@section('content')

     <style type="text/css">
        .activatie-geldig{
            display: none;
        }
        .activatie-geldig.active{
            display: block;
        }
    </style>

    <div class="col-md-12">
        {!! Form::open(['url' => '/restaurants/import', "id"=>"submit-form", 'files' => true]) !!}

            <div class="fw">
                <div class="col-md-2 no-p-l pull-left">
                    <label class="m-t-h m-b-h" for="date">Import .CSV</label>
                </div>
                <div class="col-md-10 form-border-left no-p-r pull-right">
                    <input id="import" type="file" name="import" class="form-control m-t-h m-b " required>
                </div>
            </div>

            <div class="fw">
                <button class="btn btn-primary pull-right" type="submit">Opslaan</button>
            </div>

		{!! Form::close() !!}
    </div>
@endsection

@section('scripts')
    <script type="text/javascript">
        $( document ).ready(function() {
            $(".form-save").click(function() {
                $('#submit-form').find('[type="submit"]').trigger('click');
            });
        });
    </script>
@append