<head>
<meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<link href="https://fonts.googleapis.com/css?family=Lato" rel="stylesheet">
<title>Diner Kortings Kaart</title>
<style type="text/css">
@import
 url('https://fonts.googleapis.com/css?family=Lato');
#outlook a {padding:0;}
a:link {color:#51bae2;}
a:visited {color:#51bae2;}
a:hover {color:#51bae2;}
a:active {color:#51bae2;}
a {text-decoration: none;}
a img {border:none;} 
body{width:100% !important; -webkit-text-size-adjust:100%; -ms-text-size-adjust:100%; margin:0; padding:0;} 
.ExternalClass {width:100%;}
#backgroundTable {margin:0; padding:0; width:100% !important; }
img {outline:none; text-decoration:none; -ms-interpolation-mode: 
bicubic;} 
span.yshortcuts { color:#000; background-color:none; border:none;}
span.yshortcuts:hover,
span.yshortcuts:active,
span.yshortcuts:focus {color:#000; background-color:none; border:none;}
.full-width-bg{width:100% !important; }
@media only screen and (max-width: 480px), only screen and (max-device-width: 480px) 
{
*[class=hide] { display: none !important; }
*[class=table] { width: 100% !important; }
*[class=table50] { width: 50% !important; padding-left:20px; padding-right:20px; 
}
*[class=table70] { width: 90% !important; }
*[class=tableCenter] { width: 100% !important; text-align:center; padding-bottom:20px; }
*[class=tablePad] { padding:20px; width: 100% !important; }
*[class=tableWhiteBackground] { width: 100% !important; background-color:#ffffff; }
*[class=table90] { width: 90% !important; }
*[class=tableVisibleFeature] {display:block !important;  margin:auto !important; width:100% !important; height:auto !important; max-height:inherit !important; overflow:visible 
!important;}
}
</style>
</head>
<body bgcolor="#F2F1EF" style="-webkit-font-smoothing: antialiased; width:100% !important; background:#F2F1EF; -webkit-text-size-adjust:none;">




<table class="table" width="100%" border="0" cellpadding="0" cellspacing="0" bgcolor="#F7F7F7">
	<tbody><tr>
		<td class="tableWhiteBackground" style="border-collapse: collapse;" width="100%" bgcolor="#F2F1EF" valign="top" align="center">
		<table class="table" width="600" border="0" cellpadding="0" cellspacing="0">
			<tbody><tr>
				
<td class="table" width="600" style="border-collapse: collapse;" bgcolor="#F2F1EF" align="center" height="85" valign="middle"><br/><br/><a href="http://www.dinerkortingskaart.nl/" target="_blank">
<img style="outline:none; text-decoration:none; -ms-interpolation-mode: bicubic; display:block;" src="https://dkk.mediacompanion.nl/public/images/email/logo.png" width="160" border="0" alt=""></a><br/><br/</td>
			</tr>
		</tbody></table>
		<table class="table" width="600" border="0" cellpadding="0" cellspacing="0">
			<tbody><tr>
				
<td class="table" style="border-collapse: collapse;" width="455" height="60" bgcolor="#FFFFFF" align="center" valign="middle"><img class="table" style="outline:none; text-decoration:none; -ms-interpolation-mode: bicubic; display:block;" src="https://dkk.mediacompanion.nl/public/images/email/spacer.gif" width="600" height="60" alt=""></td>
			</tr>
		</tbody></table>
		<table class="table90" width="600" border="0" cellpadding="0" cellspacing="0">
			<tbody><tr>
				
<td class="hide" style="border-collapse: collapse;" width="35" height="2" bgcolor="#FFFFFF"><img class="table" style="outline:none; text-decoration:none; -ms-interpolation-mode: bicubic; display:block;" src="https://dkk.mediacompanion.nl/public/images/email/spacer.gif" width="35" height="2" alt=""></td>
				<td class="table" style="border-collapse: collapse; line-height:23px;" width="385" height="2" bgcolor="#FFFFFF" align="left" valign="top">
				
<center><font style="text-decoration:none; color:#01363D; letter-spacing:1px; font-size:18px; font-family:arial, helvetica, sans-serif;">
				Wachtwoord herstellen
                  </font></center><br>
				<font style="text-decoration:none; color:#01363C; font-size:15px; font-family:arial, helvetica, sans-serif;">
                  Beste {{$user->fname}},<br/><br/>
Via de mobiele applicatie van Diner Kortings Kaart is een wachtwoord herstel aangevraagd. U kunt uw wachtwoord opnieuw instellen binnen de mobiele applicatie middels de volgende code:<br/><br/>
<strong style="font-size: 18px;">{{$user->password_recovery}}</strong><br/><br/>
Deze code is 24 uur actief, na deze periode zal het aanvraag proces opnieuw aangeroepen moeten worden.
<br/><br/>
Mocht u geen aanvraag hebben gedaan voor het opnieuw instellen van uw wachtwoord, dan kunt u deze mail negeren.
				</font>

				<br><br>
				<center>
                  </center>
			
				
				<br><br>
				
				<font style="text-decoration:none; color:#01363C; font-size:15px; font-family:arial, helvetica, sans-serif;">
				Met vriendelijke groet,

				<br><br>
				
				Het Diner Kortings Kaart team
				<br/>
				
				</font>
			
				<br>
				</td>
				<td class="hide" style="border-collapse: collapse;" width="35" height="2" bgcolor="#FFFFFF">
<img class="table" style="outline:none; text-decoration:none; -ms-interpolation-mode: bicubic; display:block;" src="https://dkk.mediacompanion.nl/public/images/email/spacer.gif" width="35" height="2" alt=""></td>
			</tr>
		</tbody></table>
		<table class="table" width="600" border="0" cellpadding="0" cellspacing="0">
			<tbody><tr>
				<td class="table" style="border-collapse: collapse;" width="455" height="31" bgcolor="#FFFFFF">
<img class="table" style="outline:none; text-decoration:none; -ms-interpolation-mode: bicubic; display:block;" src="https://dkk.mediacompanion.nl/public/images/email/spacer.gif" width="600" height="31" alt=""></td>
			</tr>
		</tbody></table>
		<table class="table" width="600" border="0" cellpadding="0" cellspacing="0">
			<tbody><tr>
				<td class="table" style="border-collapse: collapse;" width="455" height="47" bgcolor="#F2F1EF">
<img class="table" style="outline:none; text-decoration:none; -ms-interpolation-mode: bicubic; display:block;" src="https://dkk.mediacompanion.nl/public/images/email/spacer.gif" width="600" height="47" alt=""></td>
			</tr>
		</tbody></table>
		<table class="table" width="600" border="0" cellpadding="0" cellspacing="0">
			<tbody><tr>
				
				
<td class="table" style="border-collapse: collapse; line-height:24px;" width="296" height="2" bgcolor="#F2F1EF" align="center" valign="top">
				<a href="http://www.dinerkortingskaart.nl/" target="_blank"><img src="https://dkk.mediacompanion.nl/public/images/email/logo_small.png" width="50" border="0" alt=""></a>
		
				
<br>
		
				<font style="color:#01363C; font-size:10px; font-family:arial, helvetica, sans-serif;">Diner Kortingskaart BV, Asterweg 20 B2, 1031 HN Amsterdam</font>
				<br>
				
				
				</td>
				
			</tr>
		</tbody></table>
		</td>
	</tr>
</tbody></table>

<table class="hide" width="600" border="0" cellpadding="0" cellspacing="0">
	<tbody><tr>
		<td class="hide" bgcolor="#F2F1EF">
			<table cellpadding="0" cellspacing="0" border="0" align="center" width="600">
				<tbody><tr>
					<td bgcolor="#F2F1EF" style="background-color:#F2F1EF; line-height:15px; height:15px; min-width: 600px;">
						
<img src="https://dkk.mediacompanion.nl/public/images/email/spacer.gif" height="15" width="600" style="max-height:15px; min-height:15px; display:block; width:600px; min-width:640px;">
					</td>
				</tr>
			</tbody></table>
		</td>
	</tr>
</tbody></table>
<div style="display:none; white-space:nowrap; font:15px courier; line-height:0;">
&nbsp;
 &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; 
&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; 
&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
</div>

</body>