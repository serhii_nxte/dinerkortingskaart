@extends('layouts.app')

@section('pageTitle')
    Kaart aanpassen
@endsection

@section('pageActions')
    <div class="col-md-12">
        <a href="#" class="btn btn-primary form-save pull-right">Opslaan</a>
        <a class="pull-right m-r-h m-t-q" href="/cards">< Naar kaart overzicht</a>
    </div>
@endsection

@section('content')
<style type="text/css">
        .activatie-geldig{
            display: none;
        }
        .activatie-geldig.active{
            display: block;
        }
    </style>
    <div class="col-md-12">
        {!! Form::open(["url" => "/cards/$card->id", "id"=>"submit-form", "method"=>"PATCH"]) !!}

            <div class="fw">
                <div class="col-md-2 no-p-l pull-left">
                    <label class="m-t-h m-b-h">Kaartnummer / actvatiecode</label>
                </div>
                <div class="col-md-10 form-border-left no-p-r pull-right">
                    <input id="name" type="text" placeholder="Wordt automatisch gegenereerd." class="form-control m-t-h m-b-h" value="{{ $card->activation }}" disabled>
                </div>
            </div>

            <div class="fw">
                <div class="col-md-2 no-p-l pull-left">
                    <label class="m-t-h m-b-h" for="date">Geldig tot</label>
                </div>
                <div class="col-md-10 form-border-left no-p-r pull-right">

                    @if(!is_null($card->expiration))

                        <input id="date" data-date-format="dd-mm-yyyy" type="text" name="date" class="form-control geldig-tot m-t-h m-b-h datepicker" value="{{ date('d-m-Y', strtotime($card->expiration)) }}" required>

                        <label><input type="checkbox" name="activatie_geldig" id="activatie-geldig" > Geldig vanaf activatie</label>
                    @else
                        <input id="date" data-date-format="dd-mm-yyyy" type="text" name="date" class="form-control geldig-tot m-t-h m-b-h datepicker" disabled>

                        <label><input type="checkbox" name="activatie_geldig" id="activatie-geldig" checked> Geldig vanaf activatie</label>

                    @endif

                    

                </div>
            </div>

            <div class="fw activatie-geldig @if(is_null($card->expiration)) active @endif">
                <div class="col-md-2 no-p-l pull-left">
                    <label class="m-t-h m-b-h" for="date">Aantal maanden geldig</label>
                </div>
                <div class="col-md-10 form-border-left no-p-r pull-right">
                    <input id="period" type="number" name="period" placeholder="12" class="form-control m-t-h m-b-h" value="{{ $card->period }}">
                </div>
            </div>

            <div class="fw">
                <div class="col-md-2 no-p-l pull-left">
                    <label class="m-t-h m-b-h" for="name">Partner</label>
                </div>
                <div class="col-md-10 form-border-left no-p-r pull-right">
                    <select name="partner" class="chzn-select form-control">
                        
                        <option value="-" @if(!$card->partner_id) selected @endif>Geen partner</option>
                        @foreach($partners as $partner)
                            <option value="{{$partner->id}}" @if($card->partner_id == $partner->id) selected @endif>{{$partner->name}} ({{$partner->id}})</option>
                        @endforeach
                    </select>
                    <small class="help-block bg-info">Kies 'geen partner' als er geen partner gekoppeld dient te worden!</small>
                </div>
            </div>

            <div class="fw">
                <div class="col-md-2 no-p-l pull-left">
                    <label class="m-t-h m-b-h" for="name">Gebruiker</label>
                </div>
                <div class="col-md-10 form-border-left no-p-r pull-right">
                    <select name="gebruiker" class="chzn-select form-control">
                        <option value="-" @if(!$card->user_id) selected @endif>Geen gebruiker</option>
                        @foreach($users as $user)
                            <option value="{{$user->id}}" @if($card->user_id == $user->id) selected @endif>{{$user->email}} ({{$user->fname}} {{$user->lname}})</option>
                        @endforeach
                    </select>
                    <small class="help-block bg-info">Kies 'geen gebruiker' als er geen partner gekoppeld dient te worden!</small>
                </div>
            </div>

            <div class="fw">
                <button class="btn btn-primary pull-right" type="submit">Opslaan</button>
            </div>

        {!! Form::close() !!}
    </div>
@endsection

@section('scripts')
    <script type="text/javascript">
        $( document ).ready(function() {

            $(".datepicker").datepicker({ dateFormat: 'dd-mm-yy' });

            $(".chzn-select").chosen();

            // course picker
            $('#activatie-geldig').change(function() {
                if($(this).is(":checked")) {
                    $('.geldig-tot').prop('disabled', true).prop('required', false);
                    $('.activatie-geldig').addClass('active');
                    $('#period').prop('disabled', false).prop('required', true);
                }else{
                    $('.geldig-tot').prop('disabled', false).prop('required', true);
                    $('.activatie-geldig').removeClass('active');
                    $('#period').prop('disabled', true).prop('required', false);
                }        
            });

            // course picker
            $('input[type=radio][name=type]').on('change', function() {
                 switch($(this).val()) {
                     case '1':
                        $('.food-group').addClass('active');
                        $('.drink-group').removeClass('active');
                        $('.food-group').show();
                        $('.drink-group').hide();
                        $('.no-option').hide();
                        $("input[name='course']").val([]);
                        break;
                     case '0':
                        $('.food-group').removeClass('active');
                        $('.drink-group').addClass('active');
                        $('.food-group').hide();
                        $('.drink-group').show();
                        $('.no-option').hide();
                        $("input[name='course']").val([]);
                        break;
                 }
            });

            $(".form-save").click(function() {
                $('#submit-form').find('[type="submit"]').trigger('click');
            });
        });
    </script>
@append