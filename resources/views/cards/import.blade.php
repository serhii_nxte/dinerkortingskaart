@extends('layouts.app')

@section('pageTitle')
    Kaarten importeren
@endsection

@section('pageActions')
	<div class="col-md-12">
		<a href="#" class="btn btn-primary form-save pull-right">Opslaan</a>
        <a class="pull-right m-r-h m-t-q" href="/cards">< Naar kaart overzicht</a>
	</div>
@endsection

@section('content')

     <style type="text/css">
        .activatie-geldig{
            display: none;
        }
        .activatie-geldig.active{
            display: block;
        }
    </style>

    <div class="col-md-12">
        {!! Form::open(['url' => '/cards/import', "id"=>"submit-form", 'files' => true]) !!}

            <div class="fw">
                <div class="col-md-2 no-p-l pull-left">
                    <label class="m-t-h m-b-h" for="date">Import .CSV</label>
                </div>
                <div class="col-md-10 form-border-left no-p-r pull-right">
                    <input id="import" type="file" name="import" class="form-control m-t-h m-b " required>
                </div>
            </div>

            <div class="fw">
                <div class="col-md-2 no-p-l pull-left">
                    <label class="m-t-h m-b-h" for="date">Geldig tot</label>
                </div>
                <div class="col-md-10 form-border-left no-p-r pull-right">
                    
                    <label  class="m-t-h"><input type="radio" name="expiration" value="date" id="activatie-geldig" checked required> specifieke datum</label>
                    &nbsp;&nbsp;
                    <label  class="m-t-h"><input type="radio" name="expiration" value="period" id="activatie-geldig" required> vanaf activatie</label>

                </div>
            </div>

            <div class="fw">
                <div class="col-md-2 no-p-l pull-left">
                    <label class="m-t-h m-b-h" for="partner">Koppelen aan partner</label>
                </div>
                <div class="col-md-10 form-border-left no-p-r pull-right">
                    <select id="partner" name="partner" class="form-control m-t-h m-b ">
                        <option value="-">Geen partner</option>
                        <?php foreach ($partners as $partner) { ?>
                            <option value="{{$partner->id}}">{{$partner->name}}</option>
                        <?php } ?>
                    </select>
                </div>
            </div>

            <div class="fw">
                <button class="btn btn-primary pull-right" type="submit">Opslaan</button>
            </div>

		{!! Form::close() !!}
    </div>
@endsection

@section('scripts')
    <script type="text/javascript">
        $( document ).ready(function() {

            $(".datepicker").datepicker({ dateFormat: 'dd-mm-yy' });

            $(".chzn-select").chosen();

            // course picker
            $('input[type=radio][name=type]').on('change', function() {
                 switch($(this).val()) {
                     case '1':
                        $('.food-group').addClass('active');
                        $('.drink-group').removeClass('active');
                        $('.food-group').show();
                        $('.drink-group').hide();
                        $('.no-option').hide();
                        $("input[name='course']").val([]);
                        break;
                     case '0':
                        $('.food-group').removeClass('active');
                        $('.drink-group').addClass('active');
                        $('.food-group').hide();
                        $('.drink-group').show();
                        $('.no-option').hide();
                        $("input[name='course']").val([]);
                        break;
                 }
            });

            $(".form-save").click(function() {
                $('#submit-form').find('[type="submit"]').trigger('click');
            });
        });
    </script>
@append