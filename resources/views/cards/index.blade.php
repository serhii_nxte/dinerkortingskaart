@extends('layouts.app')

@section('pageTitle')
    Kaarten
@endsection

@section('pageActions')
	<div class="col-md-12">
		<p class="pull-left record-counter">{{$cards->count()}} Records found</p>
		
		<a href="/cards/create" class="btn btn-primary pull-right"><i class="fa fa-plus" aria-hidden="true"></i> Aanmaken</a>
		<a href="/cards/import" class="btn btn-primary pull-right m-r-h"><i class="fa fa-download" aria-hidden="true"></i> Importeren</a>
	</div>
@endsection

@section('content')
    <div class="col-md-12">
        <table class="table" >
        	{!! Form::open(['method' => 'GET']) !!}
        	<thead>
	        	<tr>
	        		<th class="footable-sortable">
	        			<span>Id</span>
	        		</th>
	        		<th class="footable-sortable">
	        			Kaartnummer<br/>
	        			<input type="text" class="form-control" name="cardnumber" @if(isset($_GET['cardnumber'])) value="{{$_GET['cardnumber']}}" @endif>
	        		</th>
	        		<th class="footable-sortable">
	        			Partner<br/>
	        			<select name="partner" class="form-control">
	        				<option value="-">- geen partner -</option>
	        				@foreach($partners as $partner)
	        					<option value="{{$partner->id}}" <?php if(isset($_GET['partner'])){ if($_GET['partner'] == $partner->id){ echo ' selected '; } } ?>>{{$partner->name}}</option>
	        				@endforeach
	        			</select>
	        		</th>
	        		<th class="footable-sortable">
	        			verloopdatum<br/>
	        			<input type="text" class="form-control" name="expiration" @if(isset($_GET['expiration'])) value="{{$_GET['expiration']}}" @endif>
	        		</th>
	        		<th class="footable-sortable">
	        			User Achternaam<br/>
	        			<input type="text" class="form-control" name="lname" @if(isset($_GET['lname'])) value="{{$_GET['lname']}}" @endif>
	        		</th>
	        		<th class="footable-sortable">
	        			User Email<br/>
	        			<input type="text" class="form-control" name="email" @if(isset($_GET['email'])) value="{{$_GET['email']}}" @endif>
	        		</th>
	        		<th>
	        			<input type="submit" class="btn btn-primary pull-right" value="Zoeken">
	        		</th>
	        	</tr>
        	</thead>
        	{!! Form::close() !!}
        	<tbody>
	        	@foreach($cards as $card)
	        		<tr>
	        			<td>{{$card->id}}</td>
	        			<td>{{$card->cardnumber}}</td>
	        			<td>
	        				@if($card->partner()->first())
	        					{{$card->partner->name}}
	        				@else
	        					-
	        				@endif
	        			</td>
	        			<td>@if(is_null($card->expiration)) <small>({{$card->period}} maanden na activatie)</small> @else {{ date('d-m-Y', strtotime($card->expiration))}} @endif</td>
	        			<td>
	        				@if($card->user()->first())
	        					{{$card->user->lname}} ({{$card->user->fname}})
	        				@else
	        					-
	        				@endif
	        			</td>
	        			<td>
	        				@if($card->user()->first())
	        					{{$card->user->email}}
	        				@else
	        					-
	        				@endif
	        			</td>
	        			<td>
	        				<a class="btn btn-primary pull-right" href="/cards/{{$card->id}}/edit"><i class="fa fa-pencil"></i></a>
	        			</td>
	        		</tr>
	        	@endforeach
	        </tbody>
	        <tfoot>
	            <tr>
                    <td colspan="6">
                        <ul class="pagination pull-right">
                        	{{$cards->appends($_GET)->links()}}
                        </ul>
                    </td>
                </tr>
            </tfoot>
        </table>
    </div>
@endsection

@section('scripts')
<script type="text/javascript">
	$(document).ready(function() {
		$('.ftable').footable();
	});
</script>
@append
