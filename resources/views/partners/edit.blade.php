@extends('layouts.app')

@section('pageTitle')
    Partner aanpassen
@endsection

@section('pageActions')
	<div class="col-md-12">
		<a href="#" class="btn btn-primary form-save pull-right">Opslaan</a>
        <a class="pull-right m-r-h m-t-q" href="/partners">< Terug</a>
	</div>
@endsection

@section('content')

    <div class="col-md-12">
        {!! Form::open(["url" => "/partners/$partner->id", "method"=>"PATCH", "id"=>"submit-form", "files" => true]) !!}

            <div class="fw">
                <div class="col-md-2 no-p-l pull-left">
                    <label class="m-t-h m-b-h" for="name">Naam</label>
                </div>
                <div class="col-md-10 form-border-left no-p-r pull-right">
                    <input id="name" type="text" name="name" class="form-control m-t-h m-b-h" value="{{ $partner->name }}" required>
                </div>
            </div>

            <div class="fw">
                <div class="col-md-2 no-p-l pull-left">
                    <label class="m-t-h m-b-h" for="image">Afbeelding</label>
                </div>
                <div class="col-md-10 form-border-left no-p-r pull-right">
                    <img id="image" class="user-create-placeholder m-b-h" src="/images/uploads/partners/{{$partner->image}}" style="max-width: 240px;">
                    <input id="upload-image" type="file" name="image" class="form-control">
                    <small class="help-block bg-info">Laat leeg als de afbeelding niet veranderd dient te worden!</small>
                </div>
            </div>

            <div class="fw">
                <div class="col-md-2 no-p-l pull-left">
                    <label class="m-t-h m-b-h" for="name">Tekst</label>
                </div>
                <div class="col-md-10 form-border-left no-p-r pull-right">
                    <input id="text" type="text" name="text" value="{{$partner->tekst}}" class="form-control m-t-h m-b-h">
                </div>
            </div>

            <div class="fw">
                <button class="btn btn-primary pull-right" type="submit">Opslaan</button>
            </div>

		{!! Form::close() !!}
    </div>
@endsection

@section('scripts')
    <script type="text/javascript">

        function readURL(input) {

            if (input.files && input.files[0]) {
                var reader = new FileReader();

                reader.onload = function (e) {
                    $('#image').attr('src', e.target.result);
                    $('#image').show();
                }

                reader.readAsDataURL(input.files[0]);
            }
        }

        $("#upload-image").change(function(){
            readURL(this);
        });

        $( document ).ready(function() {
            $(".form-save").click(function() {
                $('#submit-form').find('[type="submit"]').trigger('click');
            });
        });
    </script>
@append