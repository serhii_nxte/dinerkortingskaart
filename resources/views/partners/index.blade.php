@extends('layouts.app')

@section('pageTitle')
    Partners
@endsection

@section('pageActions')
	<div class="col-md-12">
		<p class="pull-left record-counter">{{$partners->count()}} Records found</p>
		
		<a href="/partners/create" class="btn btn-primary pull-right"><i class="fa fa-plus" aria-hidden="true"></i> Aanmaken</a>
		{{-- <a href="/users/import" class="btn btn-primary pull-right m-r-h"><i class="fa fa-download" aria-hidden="true"></i> Importeren</a> --}}
	</div>
@endsection

@section('content')
    <div class="col-md-12">
        <table class="table ftable table-striped footable" data-paging="true" data-filtering="true" data-sorting="true" data-state="true" data-page-size="12" >
        	<thead>
	        	<tr>
	        		<th class="footable-sortable">Id</th>
	        		<th class="footable-sortable">Partner</th>
	        		<th class="footable-sortable">Afbeelding</th>
	        		<th class="footable-sortable">Tekst</th>
	        		<th class="footable-sortable"># Actieve kaarten</th>
	        		<th></th>
	        	</tr>
        	</thead>
        	<tbody>
	        	@foreach($partners as $partner)
	        		<tr>
	        			<td>{{$partner->id}}</td>
	        			<td>{{$partner->name}}</td>
	        			<td><img src="/images/uploads/partners/{{$partner->image}}" style="max-height: 60px; max-width: 120px;"></td>
	        			<td>
	        				@if($partner->tekst)
	        					{{$partner->tekst}}
	        				@else
	        				-
	        				@endif
	        			</td>
	        			<td>{{$partner->cards()->where('expiration', '>', date('Y-m-d'))->WhereNotNull('user_id')->count()}}</td>
	        			<td>
	        				<a class="btn btn-primary pull-right" href="/partners/{{$partner->id}}/edit"><i class="fa fa-pencil"></i></a>
	        			</td>
	        		</tr>
	        	@endforeach
	        </tbody>
	        <tfoot>
	            <tr>
                    <td colspan="6">
                        <ul class="pagination pull-right"></ul>
                    </td>
                </tr>
            </tfoot>
        </table>
    </div>
@endsection

@section('scripts')
<script type="text/javascript">
	$(document).ready(function() {
		$('.ftable').footable();
	});
</script>
@append
